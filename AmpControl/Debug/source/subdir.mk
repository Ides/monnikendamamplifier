################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../source/AmpControl.cpp \
../source/ChannelControl.cpp \
../source/I2C.cpp \
../source/Parameter.cpp \
../source/ParameterControl.cpp \
../source/SPI.cpp \
../source/USART.cpp \
../source/adau1401.cpp \
../source/displayMaster.cpp \
../source/eeprom.cpp \
../source/eepromControl.cpp 

C_SRCS += \
../source/semihost_hardfault.c 

OBJS += \
./source/AmpControl.o \
./source/ChannelControl.o \
./source/I2C.o \
./source/Parameter.o \
./source/ParameterControl.o \
./source/SPI.o \
./source/USART.o \
./source/adau1401.o \
./source/displayMaster.o \
./source/eeprom.o \
./source/eepromControl.o \
./source/semihost_hardfault.o 

CPP_DEPS += \
./source/AmpControl.d \
./source/ChannelControl.d \
./source/I2C.d \
./source/Parameter.d \
./source/ParameterControl.d \
./source/SPI.d \
./source/USART.d \
./source/adau1401.d \
./source/displayMaster.d \
./source/eeprom.d \
./source/eepromControl.d 

C_DEPS += \
./source/semihost_hardfault.d 


# Each subdirectory must supply rules for building sources it contributes
source/%.o: ../source/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C++ Compiler'
	arm-none-eabi-c++ -D__LPC5411X__ -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DSDK_DEBUGCONSOLE=0 -DFSL_RTOS_FREE_RTOS -DSDK_OS_FREE_RTOS -DCPU_LPC54113J256BD64 -DCPU_LPC54113J256BD64_cm4 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -D__NEWLIB__ -I../board -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\DisplayControl\source" -I../source -I../ -I../drivers -I../device -I../CMSIS -I../component/serial_manager -I../utilities -I../utilities/utilities -I../component/lists -I../component/uart -I../startup -O0 -fno-common -g3 -Wall -c -fmessage-length=0 -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -D__NEWLIB__ -fstack-usage -specs=nano.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

source/%.o: ../source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__LPC5411X__ -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DSDK_DEBUGCONSOLE=0 -DFSL_RTOS_FREE_RTOS -DSDK_OS_FREE_RTOS -DCPU_LPC54113J256BD64 -DCPU_LPC54113J256BD64_cm4 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -D__NEWLIB__ -I../board -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\DisplayControl\source" -I../source -I../ -I../drivers -I../device -I../CMSIS -I../component/serial_manager -I../utilities -I../utilities/utilities -I../component/lists -I../component/uart -I../startup -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -D__NEWLIB__ -fstack-usage -specs=nano.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


