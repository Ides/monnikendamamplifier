#ifndef _PARAMETER_CONTROL_H_
#define _PARAMETER_CONTROL_H_

//#include <stdint.h>
//#include <stdio.h>
#include "fsl_common.h"
#include "eepromControl.h"
#include "adau1401.h"
#include "parameter.h"
//#include <map>

class ParameterControl
{
private:
	ADAU1401 *_adau1401;
	EEPROMControl *_eepromControl;
	std::map<uint16_t,Parameter> parameterMap;

	uint16_t currentParameterId = 0;
public:
	// Constructor
	ParameterControl(EEPROMControl *eepromControl, ADAU1401 *adau);
	// Destructor
	virtual ~ParameterControl();

	void LoadParameters(void);
	void SaveCurrentAsPreset(uint8_t presetIndex);
	void LoadPreset(uint8_t presetIndex);

	bool SetCurrentParameter(uint16_t parameterId);
	uint16_t GetCurrentParameter(void);
	void AddToCurrent(int16_t valueToAdd);
	void SetCurrent(int16_t valueToSet);
	uint16_t GetCurrent(void);
	uint16_t GetCurrentMax(void);
	uint16_t GetParameterValue(uint16_t parameterId);
	void SetParameterValue(uint16_t parameterId, uint16_t value);
};

#endif /* _PARAMETER_CONTROL_H_ */
