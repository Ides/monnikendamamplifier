#ifndef _CHANNELCONTROL_H_
#define _CHANNELCONTROL_H_

#include <stdint.h>
#include <stdio.h>
#include "fsl_common.h"


struct ChannelState
{
	bool active;
	bool mute;
	int selectedStream;
	bool isEditing;
};

class ChannelControl
{
private:

	ChannelState mChannelStates[8];

public:
	// Constructor
	ChannelControl();

	// Destructor
	virtual ~ChannelControl();


	uint8_t GetChannelStateByte(uint8_t channel);

};

#endif /* _CHANNELCONTROL_H_ */
