/*
 * I2C.h
 *
 *  Created on: 14 Dec 2018
 *      Author: Matthijs
 */

#ifndef I2C_H_
#define I2C_H_

#include "fsl_i2c.h"


class I2C
{
private:
	// The I2C_Type base for reference
	I2C_Type *mBase;
	// Handle for the communication
	i2c_master_handle_t I2C_Handle;


	// Callback for the transfer complete
	static void I2C_MasterCallback(I2C_Type *base, i2c_master_handle_t *handle, status_t status, void *userData);

	// Indicates if there is a transfer active
	bool I2C_TransferBusy = false;
	// Indicates of the last transfer was a success
	bool I2C_TransferSuccess = false;

public:
	// Constructor
	I2C(I2C_Type *base);
	// Destructor
	virtual ~I2C();

	status_t Transfer(i2c_master_transfer_t *transfer);

};

#endif /* I2C_H_*/
