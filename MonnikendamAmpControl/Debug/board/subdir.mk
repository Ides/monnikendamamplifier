################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../board/board.c \
../board/clock_config.c \
../board/peripherals.c \
../board/pin_mux.c 

OBJS += \
./board/board.o \
./board/clock_config.o \
./board/peripherals.o \
./board/pin_mux.o 

C_DEPS += \
./board/board.d \
./board/clock_config.d \
./board/peripherals.d \
./board/pin_mux.d 


# Each subdirectory must supply rules for building sources it contributes
board/%.o: ../board/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__NEWLIB__ -DCPU_LPC54113J256BD64 -DCPU_LPC54113J256BD64_cm4 -D__LPC5411X__ -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DSDK_DEBUGCONSOLE=0 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\drivers" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\utilities" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\device" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\CMSIS" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\component\serial_manager" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\drivers" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\utilities" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\device" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\CMSIS" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\component\serial_manager" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\board" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\source" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\startup" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -D__NEWLIB__ -fstack-usage -specs=nano.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


