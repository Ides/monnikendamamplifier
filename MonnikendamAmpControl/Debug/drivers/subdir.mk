################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../drivers/fsl_clock.c \
../drivers/fsl_common.c \
../drivers/fsl_ctimer.c \
../drivers/fsl_flexcomm.c \
../drivers/fsl_gint.c \
../drivers/fsl_gpio.c \
../drivers/fsl_i2c.c \
../drivers/fsl_power.c \
../drivers/fsl_reset.c \
../drivers/fsl_spi.c \
../drivers/fsl_usart.c 

OBJS += \
./drivers/fsl_clock.o \
./drivers/fsl_common.o \
./drivers/fsl_ctimer.o \
./drivers/fsl_flexcomm.o \
./drivers/fsl_gint.o \
./drivers/fsl_gpio.o \
./drivers/fsl_i2c.o \
./drivers/fsl_power.o \
./drivers/fsl_reset.o \
./drivers/fsl_spi.o \
./drivers/fsl_usart.o 

C_DEPS += \
./drivers/fsl_clock.d \
./drivers/fsl_common.d \
./drivers/fsl_ctimer.d \
./drivers/fsl_flexcomm.d \
./drivers/fsl_gint.d \
./drivers/fsl_gpio.d \
./drivers/fsl_i2c.d \
./drivers/fsl_power.d \
./drivers/fsl_reset.d \
./drivers/fsl_spi.d \
./drivers/fsl_usart.d 


# Each subdirectory must supply rules for building sources it contributes
drivers/%.o: ../drivers/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__NEWLIB__ -DCPU_LPC54113J256BD64 -DCPU_LPC54113J256BD64_cm4 -D__LPC5411X__ -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DSDK_DEBUGCONSOLE=0 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\drivers" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\utilities" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\device" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\CMSIS" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\component\serial_manager" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\drivers" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\utilities" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\device" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\CMSIS" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\component\serial_manager" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\board" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\source" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl" -I"C:\Users\Matthijs\Documents\MCU Expresso Repos\MonnikendamAmplifier\MonnikendamAmpControl\startup" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -D__NEWLIB__ -fstack-usage -specs=nano.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


