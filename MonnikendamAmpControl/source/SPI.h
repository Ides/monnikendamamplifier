/* SPI.h
 *
 *  Created on: 5 Jan 2019
 *      Author: Matthijs
 */

#ifndef SPI_H_
#define SPI_H_

#include "fsl_spi.h"


class SPI
{
private:
	// The SPI_Type base for reference
	SPI_Type *mBase;
	// Handle for the communication
	spi_master_handle_t SPI_Handle;


	// Callback for the transfer complete
	static void SPI_MasterCallback(SPI_Type *base, spi_master_handle_t *handle, status_t status, void *userData);

public:
	// Constructor
	SPI(SPI_Type *base, spi_master_config_t *masterConfig);
	// Destructor
	virtual ~SPI();

	status_t Transfer(uint8_t *tx_data, uint8_t *rx_data, size_t dataSize, bool eof, spi_ssel_t ssel);

	// Indicates if there is a transfer active
	volatile bool SPI_TransferBusy = false;
	// Indicates of the last transfer was a success
	volatile bool SPI_TransferSuccess = false;
};

#endif /* SPI_H_ */

