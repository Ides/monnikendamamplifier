/**
 * @file    eeprom.c
 * @brief   EEPROM communication file for the on-board 2Mbit chip
 */

/* This is a template for board specific configuration created by MCUXpresso IDE Project Wizard.*/

#include <adau1401.h>
#include <stdint.h>
#include "fsl_debug_console.h"
#include "fsl_gpio.h"

ADAU1401::ADAU1401(I2C_Type *i2c_base, uint8_t slaveAddress)
{
	_i2c = new I2C(i2c_base);
	_slaveAddress = slaveAddress;
}

ADAU1401::~ADAU1401()
{
	delete _i2c;
	_i2c = NULL;
}

int ADAU1401::LoadProgramRAM(uint8_t *Buffer, uint16_t bufferLength)
{
//	uint8_t ProgramRAMBuffer[5120] = {0};

//	EEPROM_Read(Base + 0x1000, ProgramRAMBuffer, 5120);

//	Matrix_I2C_Write(0x0400, ProgramRAMBuffer, 5120);

	if(bufferLength % 5 != 0)
		return -1;

	int result = 0;
	for(uint16_t i = 0; i < bufferLength / 5; i++)
	{
		// Write with an offset of 1024, becasue that is where the Program RAM starts
		result = Write(1024 + i, Buffer + (5*i), 5);
		if(result != 0)
			return result;
	}
	return 0;
}

int ADAU1401::LoadParameterRAM(uint8_t *Buffer, uint16_t bufferLength)
{
//	uint8_t ParameterRAMBuffer[4096] = {0};

//	EEPROM_Read(Base + 0x5000, ParameterRAMBuffer, 4096);

	if(bufferLength % 4 != 0)
		return -1;

	int result = 0;
	for(uint16_t i = 0; i < bufferLength / 4; i++)
	{
		result = Write(i, Buffer + (4*i), 4);
		if(result != 0)
			return result;
	}

	return 0;
}

int ADAU1401::LoadHardwareRegisters(uint8_t *Buffer, uint8_t bufferLength)
{
//	uint8_t HardwareRegistersBuffer[24] = {0};

	if(bufferLength != 24)
		return -1;

//	EEPROM_Read(Base + 0x2400, HardwareRegistersBuffer, 24);

	Write(0x081C, Buffer, 24);
/*
	int result = 0;
	for(uint16_t i = 0; i < 24; i++)
	{
		//result = Matrix_I2C_Write(0x081C, R3_HWCONFIGURATION_IC_1_Default, 24);
		if(result != 0)
			return result;
	}
	*/
	return 0;
}

int ADAU1401::SetReset(bool reset)
{
	GPIO_PinWrite(GPIO, 0, 22, reset ? 0 : 1);
	return 0;
}

int ADAU1401::DisableDSPCore()
{
	// Holds the value for the DSP Core Control Register
	uint8_t DSPCoreCtrlReg[2] = { 0 };

	// First read the current value of the Core Reg
	Read(0x081C, DSPCoreCtrlReg, 2);

	// Reset bit[2] to disable the DSP Core
	DSPCoreCtrlReg[1] &= ~0x04;

	// Write the altered value back
	Write(0x081C, DSPCoreCtrlReg, 2);

	/*
	int result = 0;
	for(uint16_t i = 0; i < 24; i++)
	{
		//result = Matrix_I2C_Write(0x081C, R0_COREREGISTER_IC_1_Default, 2);
		if(result != 0)
			return result;
	}
	*/
	return 0;
}

int ADAU1401::EnableDSPCore()
{
	// Holds the value for the DSP Core Control Register
	uint8_t DSPCoreCtrlReg[2] = { 0 };

	// First read the current value of the Core Reg
	Read(0x081C, DSPCoreCtrlReg, 2);

	// Set bit[2] to enable the DSP Core
	DSPCoreCtrlReg[1] |= 0x04;

	// Write the altered value back
	Write(0x081C, DSPCoreCtrlReg, 2);
	/*
	int result = 0;
	for(uint16_t i = 0; i < 24; i++)
	{
		//result = Matrix_I2C_Write(0x081C, R4_COREREGISTER_IC_1_Default, 2);
		if(result != 0)
			return result;
	}
	*/


	return 0;
}

int ADAU1401::SetParameter(uint16_t ParameterAddress, uint32_t Value)
{
	int result = 0;
	uint8_t value[4] = { 0 };
	value[0] = (uint8_t)(Value >> 24) & 0xFF;
	value[1] = (uint8_t)(Value >> 16) & 0xFF;
	value[2] = (uint8_t)(Value >> 8) & 0xFF;
	value[3] = (uint8_t)(Value & 0xFF);

	result = Write(ParameterAddress, value, 4);
	return result;
}

int ADAU1401::SetParameterSafe(uint16_t ParameterAddress, uint32_t Value)
{
	int result = 0;

	EnsureSafeLoadIsComplete();

	// Load the parameter values to the first SafeLoad register
	result = SetSafeLoadRegister(0, ParameterAddress, Value);


	InitiateSafeLoad();

	return result;
}

void ADAU1401::EnsureSafeLoadIsComplete(void)
{
	uint8_t DSPCoreCtrlReg[2] = { 0 };

	// Read the current value of the Core Reg
	Read(0x081C, DSPCoreCtrlReg, 2);

	// If the IST bit (Initiate Safe Transfer) is set, we need to wait for it to be cleared by the ADAU1401
	while((DSPCoreCtrlReg[1] & 0x20) != 0)
		Read(0x081C, DSPCoreCtrlReg, 2);
}

void ADAU1401::InitiateSafeLoad(void)
{
	// Holds the value for the DSP Core Control Register
	uint8_t DSPCoreCtrlReg[2] = { 0 };

	// First read the current value of the Core Reg
	Read(0x081C, DSPCoreCtrlReg, 2);

	// Set bit[5] (IST) to Initiate a Safe Transfer
	DSPCoreCtrlReg[1] |= 0x20;

	// Write the altered value back
	Write(0x081C, DSPCoreCtrlReg, 2);
}

int ADAU1401::SetSafeLoadRegister(uint8_t SafeLoadRegisterIndex, uint16_t ParameterAddress, uint32_t Value)
{
	// The safeload address registeres are from 0x0815 to 0x0819
	// The corresponding safeload value registers are from 0x0810 to 0x0814

	if(SafeLoadRegisterIndex > 4)
		SafeLoadRegisterIndex = 4;

	// Holds the Address part of this safeload
	uint8_t address[2] = { 0 };
	// Holds the Value part of this safeload
	uint8_t value[5] = { 0 };

	// Convert the Address
	address[0] = (uint8_t)(ParameterAddress >> 8) & 0x0F;
	address[1] = (uint8_t)(ParameterAddress & 0xFF);

	// Convert the Value
	value[0] = (uint8_t)0x00;
	value[1] = (uint8_t)(Value >> 24) & 0xFF;
	value[2] = (uint8_t)(Value >> 16) & 0xFF;
	value[3] = (uint8_t)(Value >> 8) & 0xFF;
	value[4] = (uint8_t)(Value & 0xFF);


	// Set the Address to the safeload register
	Write(SAFELOAD_ADDRESS_0 + SafeLoadRegisterIndex, address, 2);
	// Set the Value to the safeload register
	Write(SAFELOAD_VALUE_0 + SafeLoadRegisterIndex, value, 5);

	return 0;
}

int ADAU1401::Write(uint16_t SubAddress, uint8_t *buffer, size_t length)
{
	i2c_master_transfer_t masterXfer = {0};
	status_t retVal = kStatus_Fail;

    masterXfer.slaveAddress = _slaveAddress;
    masterXfer.direction = kI2C_Write;
    masterXfer.subaddress = (uint32_t)SubAddress;
    masterXfer.subaddressSize = 2;
    masterXfer.data = buffer;
    masterXfer.dataSize = length;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    retVal = _i2c->Transfer(&masterXfer);
    /* Send master non-blocking data to slave */
    //retVal = I2C_MasterTransferNonBlocking(I2C1, &g_Matrix_I2C_Handle, &masterXfer);


    if (retVal != kStatus_Success)
        return -1;

	return 0;
}

int ADAU1401::Read(uint16_t SubAddress, uint8_t *buffer, size_t length)
{
	i2c_master_transfer_t masterXfer = {0};
	status_t retVal = kStatus_Fail;

    masterXfer.slaveAddress = _slaveAddress;
    masterXfer.direction = kI2C_Read;
    masterXfer.subaddress = (uint32_t)SubAddress;
    masterXfer.subaddressSize = 2;
    masterXfer.data = buffer;
    masterXfer.dataSize = length;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    /* Send master non-blocking data to slave */
    //retVal = I2C_MasterTransferNonBlocking(I2C1, &g_Matrix_I2C_Handle, &masterXfer);
    retVal = _i2c->Transfer(&masterXfer);

    if (retVal != kStatus_Success)
        return -1;

    return 0;
}
