#include <ParameterControl.h>
#include <stdint.h>
#include "fsl_debug_console.h"
#include "fsl_gpio.h"
#include <map>

// Constructor
ParameterControl::ParameterControl(EEPROMControl *eepromControl, ADAU1401 *adau)
{
	_adau1401 = adau;
	_eepromControl = eepromControl;
}

ParameterControl::~ParameterControl()
{
	_adau1401 = NULL;
	_eepromControl = NULL;
}

void ParameterControl::LoadParameters()
{
	ParameterDescriptor desc;

	uint8_t parameterCount = _eepromControl->GetParameterCount();
	uint8_t i;

	for(i = 0; i < parameterCount; i++)
	{
		desc = _eepromControl->GetParameterDescriptor(i);
		Parameter p = Parameter(_adau1401, &desc);
		parameterMap.insert(std::pair<uint16_t, Parameter>(desc.parameter_id, p));
		//ParameterList.push_back(p);
	}
}

void ParameterControl::LoadPreset(uint8_t presetIndex)
{
	std::map<uint16_t, uint16_t> presetMap = _eepromControl->GetPresetValues(presetIndex);

//	uint16_t presetSize = presetMap.size();
//	uint16_t paramSize = parameterMap.size();

	if(presetMap.size() != parameterMap.size())
		return;

	/*
	for (std::pair<uint16_t, Parameter> x : parameterMap)
	{
		x.second.SetValue(presetMap[x.first]);
	}
	*/

	for(std::pair<uint16_t, uint16_t> x : presetMap)
	{
		SetParameterValue(x.first, x.second);
	}
}

void ParameterControl::SaveCurrentAsPreset(uint8_t presetIndex)
{
	std::map<uint16_t, uint16_t> presetMap;

	uint16_t key;
	uint16_t value;
	for (std::pair<uint16_t, Parameter> x : parameterMap)
	{
		key = x.first;
		value = x.second.GetValue();
		presetMap.insert(std::pair<uint16_t, uint16_t>(key, value));
	}

	_eepromControl->SetPresetValues(presetIndex, presetMap);
}

// Sets the current selected parameter to control
bool ParameterControl::SetCurrentParameter(uint16_t parameterId)
{
	if(parameterMap.count(parameterId) > 0)
	{
		currentParameterId = parameterId;
		return true;
	}

	return false;
}

// Sets the current selected parameter to control
uint16_t ParameterControl::GetCurrentParameter(void)
{
	return currentParameterId;
}

// Adds the given value to the current selected parameter
void ParameterControl::AddToCurrent(int16_t valueToAdd)
{
	if(parameterMap.count(currentParameterId) == 0)
		return;

	parameterMap[currentParameterId].AddToValue(valueToAdd);
}

// Sets the value of the current selected parameter to the given value
void ParameterControl::SetCurrent(int16_t valueToSet)
{
	if(parameterMap.count(currentParameterId) == 0)
		return;

	parameterMap[currentParameterId].SetValue(valueToSet);
}

// Gets the value of the current selected parameter
uint16_t ParameterControl::GetCurrent(void)
{
	if(parameterMap.count(currentParameterId) == 0)
		return 0;

	return parameterMap[currentParameterId].GetValue();
}

// Gets the maximum value of the current selected parameter
uint16_t ParameterControl::GetCurrentMax(void)
{
	if(parameterMap.count(currentParameterId) == 0)
		return 0;

	return parameterMap[currentParameterId].GetMax();
}

uint16_t ParameterControl::GetParameterValue(uint16_t parameterId)
{
	if(parameterMap.count(parameterId) == 0)
		return 0;

	return parameterMap[parameterId].GetValue();
}

void ParameterControl::SetParameterValue(uint16_t parameterId, uint16_t value)
{
	if(parameterMap.count(parameterId) == 0)
		return;

	parameterMap[parameterId].SetValue(value);
}
