#ifndef _DISPLAY_MASTER_H_
#define _DISPLAY_MASTER_H_

#include <stdint.h>
#include <stdio.h>
#include "fsl_common.h"
#include "I2C.h"

class DisplayMaster
{
private:
	uint8_t _slaveAddress;
	I2C *_i2c;

	int Write(uint8_t SubAddress, uint8_t *buffer, size_t length);
	int Read(uint8_t SubAddress, uint8_t *buffer, size_t length);
public:
	// Constructor
	DisplayMaster(I2C_Type *i2c_base, uint8_t slaveAddress);
	// Destructor
	virtual ~DisplayMaster();

	int16_t GetEncoderValue(void);
	void SetEncoderValue(uint16_t value);
	uint32_t GetButtonStates();
};

#endif /* _DISPLAY_MASTER_H_ */
