/**
 * @file    eeprom.c
 * @brief   EEPROM communication file for the on-board 2Mbit chip
 */

/* This is a template for board specific configuration created by MCUXpresso IDE Project Wizard.*/

#include <ChannelControl.h>
#include <stdint.h>
#include "fsl_debug_console.h"
#include "fsl_gpio.h"


// Constructor
ChannelControl::ChannelControl()
{
	for(uint8_t i = 0; i < 8; i++)
	{
		mChannelStates[i].active = false;
		mChannelStates[i].mute = true;
		mChannelStates[i].selectedStream = 0;
		mChannelStates[i].isEditing = false;
	}

}

ChannelControl::~ChannelControl()
{

}

uint8_t ChannelControl::GetChannelStateByte(uint8_t channel)
{

}
