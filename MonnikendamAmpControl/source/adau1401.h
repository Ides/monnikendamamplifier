#ifndef _ADAU1401_H_
#define _ADAU1401_H_

#include <stdint.h>
#include <stdio.h>
#include "fsl_common.h"
#include "I2C.h"


#define SAFELOAD_VALUE_0	0x0810
#define SAFELOAD_VALUE_1	0x0811
#define SAFELOAD_VALUE_2	0x0812
#define SAFELOAD_VALUE_3	0x0813
#define SAFELOAD_VALUE_4	0x0814
#define SAFELOAD_ADDRESS_0	0x0815
#define SAFELOAD_ADDRESS_1	0x0816
#define SAFELOAD_ADDRESS_2	0x0817
#define SAFELOAD_ADDRESS_3	0x0818
#define SAFELOAD_ADDRESS_4	0x0819

class ADAU1401
{
private:
	uint8_t _slaveAddress;
	I2C *_i2c;

	void EnsureSafeLoadIsComplete(void);
	void InitiateSafeLoad(void);
	int SetSafeLoadRegister(uint8_t SafeLoadRegisterIndex, uint16_t ParameterAddress, uint32_t Value);
	int Write(uint16_t SubAddress, uint8_t *buffer, size_t length);
	int Read(uint16_t SubAddress, uint8_t *buffer, size_t length);
public:
	// Constructor
	ADAU1401(I2C_Type *i2c_base, uint8_t slaveAddress);
	// Destructor
	virtual ~ADAU1401();

	int SetReset(bool reset);
	int LoadProgramRAM(uint8_t *Buffer, uint16_t bufferLength);
	int LoadParameterRAM(uint8_t *Buffer, uint16_t bufferLength);
	int LoadHardwareRegisters(uint8_t *Buffer, uint8_t bufferLength);
	int DisableDSPCore(void);
	int EnableDSPCore(void);
	int SetParameter(uint16_t ParameterAddress, uint32_t Value);
	int SetParameterSafe(uint16_t ParameterAddress, uint32_t Value);


};

#endif /* _ADAU1401_H_ */
