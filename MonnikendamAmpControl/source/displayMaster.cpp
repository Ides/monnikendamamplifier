/**
 * @file    eeprom.c
 * @brief   EEPROM communication file for the on-board 2Mbit chip
 */

/* This is a template for board specific configuration created by MCUXpresso IDE Project Wizard.*/

#include <displayMaster.h>
#include <stdint.h>
#include "fsl_debug_console.h"
#include "fsl_gpio.h"

DisplayMaster::DisplayMaster(I2C_Type *i2c_base, uint8_t slaveAddress)
{
	_i2c = new I2C(i2c_base);
	_slaveAddress = slaveAddress;
}

DisplayMaster::~DisplayMaster()
{
	delete _i2c;
	_i2c = NULL;
}

int16_t DisplayMaster::GetEncoderValue(void)
{
	uint8_t buffer[2];

	Read(0x31, buffer, 2);

	return buffer[0] << 8 | buffer[1];
}

void DisplayMaster::SetEncoderValue(uint16_t value)
{
	uint8_t buffer[2];
	buffer[0] = (value >> 8) & 0xFF;
	buffer[1] = value & 0xFF;

	Write(0x31, buffer, 2);
}

uint32_t DisplayMaster::GetButtonStates()
{
	uint8_t buffer[4];

	// Read the button states
	Read(0x10, buffer, 4);

	// Write the value we got back, to clear those bits again in the display
	// We write it to 0x20, because that is the clear register offset for the button states
    uint32_t delay = 0x1000;
    while(--delay){;}

	Write(0x20, buffer, 4);

	return buffer[0] << 24 | buffer[1] << 16 | buffer[2] << 8 | buffer[3];
}

int DisplayMaster::Write(uint8_t SubAddress, uint8_t *buffer, size_t length)
{
	i2c_master_transfer_t masterXfer = {0};
	status_t retVal = kStatus_Fail;

    masterXfer.slaveAddress = _slaveAddress;
    masterXfer.direction = kI2C_Write;
    masterXfer.subaddress = (uint32_t)SubAddress;
    masterXfer.subaddressSize = 1;
    masterXfer.data = buffer;
    masterXfer.dataSize = length;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    retVal = _i2c->Transfer(&masterXfer);
    /* Send master non-blocking data to slave */
    //retVal = I2C_MasterTransferNonBlocking(I2C1, &g_Matrix_I2C_Handle, &masterXfer);


    if (retVal != kStatus_Success)
        return -1;

    return 0;
}

int DisplayMaster::Read(uint8_t SubAddress, uint8_t *buffer, size_t length)
{
	i2c_master_transfer_t masterXfer = {0};
	status_t retVal = kStatus_Fail;

    masterXfer.slaveAddress = _slaveAddress;
    masterXfer.direction = kI2C_Read;
    masterXfer.subaddress = (uint32_t)SubAddress;
    masterXfer.subaddressSize = 1;
    masterXfer.data = buffer;
    masterXfer.dataSize = length;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    /* Send master non-blocking data to slave */
    //retVal = I2C_MasterTransferNonBlocking(I2C1, &g_Matrix_I2C_Handle, &masterXfer);
    retVal = _i2c->Transfer(&masterXfer);

    if (retVal != kStatus_Success)
        return -1;

	return 0;
}
