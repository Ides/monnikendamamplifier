/*
 * I2C.cpp
 *
 *  Created on: 14 Dec 2018
 *      Author: Matthijs
 */

#include <USART.h>
#include <stdio.h>


USART::USART(USART_Type *base, uint32_t flexcommFreq)
{
	// Store the USART Base for reference
	mBase = base;

	// Create a config
	usart_config_t config;

	// Get the deafult config
    USART_GetDefaultConfig(&config);
/*  config->baudRate_Bps = 115200U;
    config->parityMode = kUSART_ParityDisabled;
    config->stopBitCount = kUSART_OneStopBit;
    config->bitCountPerChar = kUSART_8BitsPerChar;
    config->loopback = false;
    config->enableRx = false;
    config->enableTx = false;
    config->txWatermark = kUSART_TxFifo0;
    config->rxWatermark = kUSART_RxFifo1; */

    // Enable Tx and Rx
    config.enableTx = true;
    config.enableRx = true;


    USART_Init(mBase, &config, flexcommFreq);
/*
    result = FLEXCOMM_Init(mBase, FLEXCOMM_PERIPH_USART);
    if (kStatus_Success != result)
        return

	result = USART_SetBaudRate(mBase, 115200U, flexcommFreq);
	if (kStatus_Success != result)
		return result;

    base->FIFOCFG |= USART_FIFOCFG_EMPTYTX_MASK | USART_FIFOCFG_ENABLETX_MASK;
    // Set to zero to trigger when tx empty
    base->FIFOTRIG &= ~(USART_FIFOTRIG_TXLVL_MASK);
    //base->FIFOTRIG |= USART_FIFOTRIG_TXLVL(config->txWatermark);
    base->FIFOTRIG |= USART_FIFOTRIG_TXLVLENA_MASK;

    base->FIFOCFG |= USART_FIFOCFG_EMPTYRX_MASK | USART_FIFOCFG_ENABLERX_MASK;
    // Set to zero to trigger when there is one item
    base->FIFOTRIG &= ~(USART_FIFOTRIG_RXLVL_MASK);
    //base->FIFOTRIG |= USART_FIFOTRIG_RXLVL(config->rxWatermark);
    base->FIFOTRIG |= USART_FIFOTRIG_RXLVLENA_MASK;

    base->CFG = 0x00000005;
//    base->CFG = USART_CFG_PARITYSEL(config->parityMode) | USART_CFG_STOPLEN(config->stopBitCount) |
//                USART_CFG_DATALEN(config->bitCountPerChar) | USART_CFG_LOOP(config->loopback) | USART_CFG_ENABLE_MASK;

*/
    USART_TransferCreateHandle(mBase, &mUsartHandle, USART_UserCallback, this);
/*

    memset(mUsartHandle, 0, sizeof(*mUsartHandle));
    mUsartHandle->rxState = kUSART_RxIdle;
    mUsartHandle->txState = kUSART_TxIdle;
    mUsartHandle->callback = callback;
    mUsartHandle->userData = userData;
    mUsartHandle->rxWatermark = 0;
    mUsartHandle->txWatermark = 0;

    FLEXCOMM_SetIRQHandler(base, (flexcomm_irq_handler_t)USART_TransferHandleIRQ, handle);

    // TODO: SHOULD NOT BE HARD SET TO FLEXCOMM 7!!!
    EnableIRQ(FLEXCOMM7_IRQn);
*/

	mReceiveTransfer.data = mReceiveBuffer;
	mReceiveTransfer.dataSize = mReceiveBufferSize;

	USART_TransferReceiveNonBlocking(mBase, &mUsartHandle, &mReceiveTransfer, NULL);
}

USART::~USART()
{
	// TODO Auto-generated destructor stub
}

bool USART::TryGetMessage(uint8_t *buffer, uint8_t bufferLength, uint8_t *receivedMessageLength)
{
	if(mReceiveRingBufferReadIndex != mReceiveRingBufferWriteIndex)
	{
		// Define the bytes written since the last decoded message
		uint8_t bytesReadCount = mReceiveRingBufferWriteIndex - mReceiveRingBufferReadIndex;
		uint8_t messageLength = mReceiveRingBuffer[mReceiveRingBufferReadIndex];
		// Do we have enough bytes to decode the message? We add 1 for the length byte
		if(bytesReadCount >= messageLength + 1)
		{
			// Copy all bytes to the provided buffer
			for(uint8_t i = 0; i < messageLength; i++)
			{
				// We add one to the index of the receive buffer to skip the length byte
				buffer[i] = mReceiveRingBuffer[(uint8_t)(mReceiveRingBufferReadIndex + 1 + i)];
			}

			if(receivedMessageLength)
				*receivedMessageLength = messageLength;

			mReceiveRingBufferReadIndex += messageLength + 1;
			return true;
		}
	}

	return false;
}

status_t USART::TrySendMessage(uint8_t *buffer, uint8_t length)
{
	if(length > 255)
		return -1;

	// Wait till we are done with sending the previous message
	while(mTxBusy)
		{;}

	// Set the first byte to the length of the message
	mTransmitBuffer[0] = length;
	// Cipy the buffer to the transmit buffer (with one offset)
	memcpy(&mTransmitBuffer[1], buffer, length);

	mTransmitTransfer.data = mTransmitBuffer;
	mTransmitTransfer.dataSize = length + 1;

	mTxBusy = true;

	return USART_TransferSendNonBlocking(mBase, &mUsartHandle, &mTransmitTransfer);
}

/*
status_t USART::Transfer(i2c_master_transfer_t *transfer)
{
//	return I2C_MasterTransferBlocking(mBase, transfer);
	uint16_t timeoutCounter = 0xFFFF;

	I2C_TransferBusy = true;

	status_t result = I2C_MasterTransferNonBlocking(mBase, &I2C_Handle, transfer);

	if(result != kStatus_Success)
		I2C_TransferBusy = false;

	while(I2C_TransferBusy && timeoutCounter-- > 0)
	{
	}

	if(timeoutCounter == 0)
		return -1;

	return result;
}
*/

void USART::USART_UserCallback(USART_Type *base, usart_handle_t *handle, status_t status, void *userData)
{
    if (kStatus_USART_TxIdle == status)
    {
    	((USART*)userData)->mTxBusy = false;
    }

    if (kStatus_USART_RxIdle == status)
    {
    	uint8_t ch = ((USART*)userData)->mReceiveBuffer[0];
    	// Copy the read byte to the buffer
    	//((USART*)userData)->mReceiveRingBuffer[((USART*)userData)->mReceiveRingBufferWriteIndex++] = ((USART*)userData)->mReceiveBuffer[0];
    	((USART*)userData)->mReceiveRingBuffer[((USART*)userData)->mReceiveRingBufferWriteIndex++] = ch;
    	// Keep on receiving
    	USART_TransferReceiveNonBlocking(base, handle, &((USART*)userData)->mReceiveTransfer, NULL);
    }
    if(kStatus_USART_RxError == status)
    {
    	uint8_t ch = ((USART*)userData)->mReceiveBuffer[0];
    	((USART*)userData)->mReceiveRingBuffer[((USART*)userData)->mReceiveRingBufferWriteIndex++] = ch;
    }
}
