#include <eeprom.h>
#include <stdint.h>
#include "fsl_debug_console.h"



/**
 * @brief Initialization Function to setup the communications for the EEPROM
 */
EEPROM::EEPROM(SPI *spi_master, spi_ssel_t ssel)
{
	_spi = spi_master;
	_ssel = ssel;
}

EEPROM::~EEPROM()
{

}


int EEPROM::ReadStateReg(uint8_t *StateReg)
{
	uint8_t RDSR = 0x05;

	_spi->Transfer(&RDSR, NULL, 4, false, _ssel);
	_spi->Transfer(NULL, StateReg, 1, true, _ssel);

	return 0;
}

// Enables the WREN bit in the State Register
void EEPROM::EnableWREN(void)
{
	// Local value of the State Register
	uint8_t StateReg = 0x00;

	// First we need to Enable the Write with WREN command (Write Enable)
	uint8_t TempBuf[4] = {0};
	TempBuf[0] = 0x06; // Enable Write Latch (WREN)

	_spi->Transfer(TempBuf, NULL, 1, true, _ssel);

	// Wait until we read the WEL bit as 1
	while((StateReg & 0x02) == 0)
		ReadStateReg(&StateReg);
}

// Waits until the WIP bit is cleared
void EEPROM::WaitForWIP(void)
{
	// Local value of the State Register
	// We make it 0x01 so the WIP bit is set
	uint8_t StateReg = 0x01;

	// Wait until the WIP bit (Write In Progress) is 0
	while((StateReg & 0x01) != 0)
		ReadStateReg(&StateReg);
}


int EEPROM::Read(uint32_t Address, uint16_t *Buf)
{
	uint8_t tmpBuf[2];

	Read(Address, tmpBuf, 2);

	*Buf = (uint16_t)((tmpBuf[0] << 8) + tmpBuf[1]);

	return 0;
}

int EEPROM::Read(uint32_t Address, uint32_t *Buf)
{
	uint8_t tmpBuf[4];

	Read(Address, tmpBuf, 4);

	*Buf = (uint32_t)((tmpBuf[0] << 24) + (tmpBuf[1] << 16) + (tmpBuf[2] << 8) + tmpBuf[3]);

	return 0;
}

int EEPROM::Read(uint32_t Address, uint8_t *rx_Buffer, size_t length)
{
	uint8_t ReadAddr[4] = {0};
	ReadAddr[0] = 0x03; // Read action
	ReadAddr[1] = (uint8_t)((Address >> 16) & 0xFF);
	ReadAddr[2] = (uint8_t)((Address >> 8) & 0xFF);
	ReadAddr[3] = (uint8_t)(Address & 0xFF);

	_spi->Transfer(ReadAddr, NULL, 4, false, _ssel);
	_spi->Transfer(NULL, rx_Buffer, length, true, _ssel);

	return 0;
}

// Writes the buffer data to the EEPROM at the specified address
// We have to chop the writes up because of the 256 bytes pages size (pointer increment won't cross page boundary)
int EEPROM::Write(uint32_t Address, uint8_t *tx_Buffer, size_t length)
{
	uint8_t AddressBuffer[4] = { 0 };
	uint32_t BufferOffset = 0;
	uint16_t WriteLength = 0;
	while(BufferOffset < length)
	{
		// Enable the Write function for the EEPROM
		EnableWREN();

		// Calculate the number of bytes we can put in the sector
		WriteLength = (0x100 - (Address % 0x100));
		// If we have less bytes (left) to write than we can in this buffer, truncate to the bytes left!
		if(WriteLength > (length - BufferOffset))
			WriteLength = length - BufferOffset;

		//PRINTF("EEPROM_Transfer => Address: 0x%05X; Length: 0x%03X; BuffOffset: 0x%05X\r\n", Address, WriteLength, BufferOffset);

		// Create a Buffer with the Write bit set and the Address to write to
		AddressBuffer[0] = 0x02; // Write action
		AddressBuffer[1] = (uint8_t)((Address >> 16) & 0xFF);
		AddressBuffer[2] = (uint8_t)((Address >> 8) & 0xFF);
		AddressBuffer[3] = (uint8_t)(Address & 0xFF);

		// Start the write action at the address
		_spi->Transfer(AddressBuffer, NULL, 4, false, _ssel);
		// Transfer the actual bytes
		_spi->Transfer((tx_Buffer + BufferOffset), NULL, WriteLength, true, _ssel);

		// Increment the BufferOffset and the address value
		BufferOffset += WriteLength;
		Address += WriteLength;

		// Wait for this write action to complete
		WaitForWIP();
	}
	return 0;
}
