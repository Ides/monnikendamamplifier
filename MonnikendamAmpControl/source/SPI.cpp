/*
 * SPI.cpp
 *
 *  Created on: 5 Jan 2019
 *      Author: Matthijs
 */

#include <SPI.h>

SPI::SPI(SPI_Type *base, spi_master_config_t *masterConfig)
{
	// Store the I2C Base for reference
	mBase = base;

    // The initialization of the SPI0 interface
    SPI_MasterInit(mBase, masterConfig, 48000000U);

    // Create a handle for the interrupt
    SPI_MasterTransferCreateHandle(mBase, &SPI_Handle, SPI_MasterCallback, this);
}

SPI::~SPI()
{
	// TODO Auto-generated destructor stub
}


status_t SPI::Transfer(uint8_t *tx_data, uint8_t *rx_data, size_t dataSize, bool eof, spi_ssel_t ssel)
{
    spi_transfer_t xfer = {0};
    xfer.txData = tx_data;
    xfer.rxData = rx_data;
    xfer.dataSize = dataSize;
    /* terminate frame */
    if (eof)
    {
        xfer.configFlags |= kSPI_FrameAssert;
    }

    SPI_Handle.sselNum = ssel;

    SPI_TransferBusy = true;

    /* transfer nonblocking */
    status_t result = SPI_MasterTransferNonBlocking(SPI0, &SPI_Handle, &xfer);

    if(result != kStatus_Success)
    	SPI_TransferBusy = false;


    /* until transfer ends */
    while (SPI_TransferBusy)
    {
    }

    return result;
}


// static callback function for the SPI transfer
void SPI::SPI_MasterCallback(SPI_Type *base, spi_master_handle_t *handle, status_t status, void *userData)
{
	// Transfer completed
	((SPI*)userData)->SPI_TransferBusy = false;

	// Success if status is SPI_Idle
	((SPI*)userData)->SPI_TransferSuccess = (status == kStatus_SPI_Idle);
}
