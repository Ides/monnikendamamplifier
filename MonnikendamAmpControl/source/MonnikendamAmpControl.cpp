/*
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    AmpControl.cpp
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "LPC54113.h"
#include "fsl_debug_console.h"
#include "fsl_ctimer.h"

/* TODO: insert other include files here. */

#include "spi.h"
#include "i2c.h"
#include "adau1401.h"
#include "eeprom.h"
#include "eepromControl.h"
#include "ParameterControl.h"
#include "displayMaster.h"
#include "USART.h"

#include "Definitions.h"
#include "Monnikendam v1.1_ADAU1401.h"
#include "3WayAndFullRange_ADAU1402.h"

/* TODO: insert other definitions and declarations here. */


#define ADAU1401_SLAVE_ADDRESS	0x34
#define DISPLAY_SLAVE_ADDRESS	0x49

SPI *spi_fc0;
ADAU1401 *adau1401;
EEPROM *eeprom;
EEPROMControl *eepromControl;
ParameterControl *parameterControl;
//DisplayMaster *displayMaster;
USART *displayUsart;


uint32_t buttonStates = 0x0000;
uint16_t encoderValue = 0;

void WriteParameterToEEPROM(EEPROM *eeprom, uint32_t Address, uint16_t ID, const char *name, uint16_t RamLocation, uint32_t ScaledMax, uint32_t ScaledMin, uint16_t CurrentValue, uint16_t MaxValue, bool SafeLoad);

void SendNewEncoderValueToDisplay(uint8_t newValue, uint8_t newMax);
void SendNewPresetSelectionValueToDisplay(uint8_t PresetSelection);

void SetupTimer(void);
uint32_t GetTimerTick(void);
void MiliSecondDelayBlocking(uint16_t msDelay);
void SendDisplayTextOnce(char* displayText);

void SPI_GetEEPROMConfig(spi_master_config_t *config)
{
    assert(NULL != config);

    /* Initializes the configure structure to zero. */
    memset(config, 0, sizeof(*config));

    config->enableLoopback = false;
    config->enableMaster = true;
    config->polarity = kSPI_ClockPolarityActiveHigh;
    config->phase = kSPI_ClockPhaseFirstEdge;
    config->direction = kSPI_MsbFirst;
    config->baudRate_Bps = 5000000;
    config->dataWidth = kSPI_Data8Bits;
    config->sselNum = kSPI_Ssel0;
    config->txWatermark = kSPI_TxFifo0;
    config->rxWatermark = kSPI_RxFifo1;
    config->sselPol = kSPI_SpolActiveAllLow;
    config->delayConfig.preDelay = 0U;
    config->delayConfig.postDelay = 0U;
    config->delayConfig.frameDelay = 0U;
    config->delayConfig.transferDelay = 0U;
}



void LoadTestDataToEEPROM(EEPROM *eeprom)
{
	uint8_t TestBuffer[64];

	eeprom->Read(0x00, TestBuffer, 32);

	// Magic string
	TestBuffer[0] = 'I';
	TestBuffer[1] = 'D';
	TestBuffer[2] = 'E';
	TestBuffer[3] = 'S';
	// EEPROM VERSION
	TestBuffer[4] = 1;
	// No of configs
	TestBuffer[5] = 3;
	// Default Config
	TestBuffer[6] = 0; // Monnikendam amp // DO NOT FORGET TO UPDATE THE CHECKSUM WHEN EDITING THIS!
	// Checksum (Sum of all bytes before this one MOD 0x100)
	TestBuffer[7] = 0x29;

	eeprom->Write(0x00, TestBuffer, 8);

	TestBuffer[0] = 'M';
	TestBuffer[1] = 'O';
	TestBuffer[2] = 'N';
	TestBuffer[3] = 'N';
	TestBuffer[4] = 'I';
	TestBuffer[5] = 'K';
	TestBuffer[6] = 'E';
	TestBuffer[7] = 'N';
	TestBuffer[8] = 'D';
	TestBuffer[9] = 'A';
	TestBuffer[10] = 'M';
	TestBuffer[11] = ' ';
	TestBuffer[12] = 'A';
	TestBuffer[13] = 'M';
	TestBuffer[14] = 'P';
	TestBuffer[15] = ' ';
	TestBuffer[16] = 'V';
	TestBuffer[17] = '1';
	TestBuffer[18] = '.';
	TestBuffer[19] = '0';
	TestBuffer[20] = 0;

	// Offset in memory
	TestBuffer[28] = 0x00;
	TestBuffer[29] = 0x00;
	TestBuffer[30] = 0x01;
	TestBuffer[31] = 0x00;
	eeprom->Write(0x08, TestBuffer, 32);

	TestBuffer[4] = '2';
	TestBuffer[5] = 0;
	TestBuffer[28] = 0x00;
	TestBuffer[29] = 0x02;
	TestBuffer[30] = 0x34;
	TestBuffer[31] = 0x56;
	eeprom->Write(0x28, TestBuffer, 32);

	TestBuffer[4] = '3';
	TestBuffer[5] = 0;
	TestBuffer[28] = 0x00;
	TestBuffer[29] = 0x03;
	TestBuffer[30] = 0x45;
	TestBuffer[31] = 0x67;
	eeprom->Write(0x48, TestBuffer, 32);

	///// WRITE THE MONNIKENDAM DATA TO THE EEPROM

	// We add the Monnikendam Configuration at base 0x100
	uint32_t MemOffset = 0x00000100;

	// The size of the Program Data (5120)
	TestBuffer[0] = 0x14;
	TestBuffer[1] = 0x00;
	eeprom->Write(MemOffset, TestBuffer, 2);
	MemOffset += 2;

	// The program Data
	eeprom->Write(MemOffset, Program_Data_ADAU1401, 5120);
	MemOffset += 5120;

	// the size of the parameter data (1024)
	TestBuffer[0] = 0x10;
	TestBuffer[1] = 0x00;
	eeprom->Write(0x00001502, TestBuffer, 2);
	MemOffset += 2;

	// The Parameter Data
	eeprom->Write(0x00001504, Param_Data_ADAU1401, 4096);
	MemOffset += 4096;

	// The hardware config Data (is always 24 bytes, so no need for the size parameter)
	eeprom->Write(0x00002504, R3_HWCONFIGURATION_ADAU1401_Default, 24);
	MemOffset += 24;

	// No. of Parameters
	TestBuffer[0] = 26;
	eeprom->Write(MemOffset, TestBuffer, 1);
	MemOffset += 1;

	// All the parameters
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_IN_0, "Master Volume Input 0", 0x0014, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_IN_1, "Master Volume Input 1", 0x0016, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;

	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_0, "Master Volume Output 0", 0x002A, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_1, "Master Volume Output 1", 0x002C, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_2, "Master Volume Output 2", 0x002E, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_3, "Master Volume Output 3", 0x0030, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_4, "Master Volume Output 4", 0x0032, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_5, "Master Volume Output 5", 0x0034, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_6, "Master Volume Output 6", 0x0036, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_7, "Master Volume Output 7", 0x0038, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;

	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_SELECT_OUT_0, "Output Select 0", 0x0022, 0x00000001, 0x00000000, 0x0000, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_SELECT_OUT_1, "Output Select 1", 0x0023, 0x00000001, 0x00000000, 0x0000, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_SELECT_OUT_2, "Output Select 2", 0x0024, 0x00000001, 0x00000000, 0x0000, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_SELECT_OUT_3, "Output Select 3", 0x0025, 0x00000001, 0x00000000, 0x0000, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_SELECT_OUT_4, "Output Select 4", 0x0026, 0x00000001, 0x00000000, 0x0000, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_SELECT_OUT_5, "Output Select 5", 0x0027, 0x00000001, 0x00000000, 0x0000, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_SELECT_OUT_6, "Output Select 6", 0x0028, 0x00000001, 0x00000000, 0x0000, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_SELECT_OUT_7, "Output Select 7", 0x0029, 0x00000001, 0x00000000, 0x0000, 0x0001, false);
	MemOffset += 47;

	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_0, "Output MUTE 0", 0x003A, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_1, "Output MUTE 1", 0x003C, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_2, "Output MUTE 2", 0x003E, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_3, "Output MUTE 3", 0x0040, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_4, "Output MUTE 4", 0x0042, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_5, "Output MUTE 5", 0x0044, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_6, "Output MUTE 6", 0x0046, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_7, "Output MUTE 7", 0x0048, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;

	// Set the last selected preset to none (0 = none)
	TestBuffer[0] = 0;
	eeprom->Write(MemOffset, TestBuffer, 1);
	MemOffset++;

	// Write all the ID's of the parameters to the preset store memory

	// The ID of the Master Volume Input 0
	TestBuffer[0] = 0x01;
	TestBuffer[1] = 0x00;
	eeprom->Write(MemOffset, TestBuffer, 2);
	MemOffset += 8;

	// The ID of the Master Volume Input 1
	TestBuffer[0] = 0x01;
	TestBuffer[1] = 0x01;
	eeprom->Write(MemOffset, TestBuffer, 2);
	MemOffset += 8;

	// All the other Parameters 0x0200-0x0207 & 0x0300-0x0307 & 0x0400-0x0407
	for(uint8_t j = 0; j < 3; j++)
	{
		for(uint8_t i = 0; i < 8; i++)
		{
			TestBuffer[0] = 0x02 + j;
			TestBuffer[1] = i;
			eeprom->Write(MemOffset, TestBuffer, 2);
			MemOffset += 8;
		}
	}

	///// END OF MONNIKENDAM DATA

	///// WRITE THE 3WAY DATA TO THE EEPROM

	// We add the Monnikendam Configuration at base ?
	TestBuffer[28] = 0x00;
	TestBuffer[29] = 0x02;
	TestBuffer[30] = 0x34;
	TestBuffer[31] = 0x56;
	MemOffset = 0x00023456;

	// The size of the Program Data (5120)
	TestBuffer[0] = 0x14;
	TestBuffer[1] = 0x00;
	eeprom->Write(MemOffset, TestBuffer, 2);
	MemOffset += 2;

	// The program Data
	eeprom->Write(MemOffset, Program_Data_3WAY, 5120);
	MemOffset += 5120;

	// the size of the parameter data (1024)
	TestBuffer[0] = 0x10;
	TestBuffer[1] = 0x00;
	eeprom->Write(MemOffset, TestBuffer, 2);
	MemOffset += 2;

	// The Parameter Data
	eeprom->Write(MemOffset, Param_Data_3WAY, 4096);
	MemOffset += 4096;

	// The hardware config Data (is always 24 bytes, so no need for the size parameter)
	eeprom->Write(MemOffset, R3_HWCONFIGURATION_3WAY_Default, 24);
	MemOffset += 24;

	// No. of Parameters
	TestBuffer[0] = 19;
	eeprom->Write(MemOffset, TestBuffer, 1);
	MemOffset += 1;

	// All the parameters
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_IN_0, "Master Volume Input 0", 0x0000, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_0, "Master Volume Output 0", 0x0056, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_1, "Master Volume Output 1", 0x0060, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_2, "Master Volume Output 2", 0x0058, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_3, "Master Volume Output 3", 0x005A, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_4, "Master Volume Output 4", 0x0062, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_5, "Master Volume Output 5", 0x005C, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_6, "Master Volume Output 6", 0x006C, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MASTERVOLUME_OUT_7, "Master Volume Output 7", 0x006E, 0x03000000, 0x00000800, 0x000A, 0x0064, true);
	MemOffset += 47;

	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_SELECT_OUT_6, "Output Select 6", 0x005E, 0x00000001, 0x00000000, 0x0000, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_SELECT_OUT_7, "Output Select 7", 0x005F, 0x00000001, 0x00000000, 0x0000, 0x0001, false);
	MemOffset += 47;

	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_0, "Output MUTE 0", 0x0064, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_1, "Output MUTE 1", 0x0070, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_2, "Output MUTE 2", 0x0066, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_3, "Output MUTE 3", 0x0068, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_4, "Output MUTE 4", 0x0072, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_5, "Output MUTE 5", 0x006A, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_6, "Output MUTE 6", 0x0074, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;
	WriteParameterToEEPROM(eeprom, MemOffset, PARAMETERTYPE_MUTE_OUT_7, "Output MUTE 7", 0x0076, 0x00800000, 0x00000000, 0x0001, 0x0001, false);
	MemOffset += 47;

	// Set the last selected preset to none (0 = none)
	TestBuffer[0] = 0;
	eeprom->Write(MemOffset, TestBuffer, 1);
	MemOffset++;

	// Write all the ID's of the parameters to the preset store memory
	// The ID of the Master Volume Input 0
	TestBuffer[0] = 0x01;
	TestBuffer[1] = 0x00;

	eeprom->Write(MemOffset, TestBuffer, 2);
	MemOffset += 8;


	// All Volume Output Parameters 0x0200-0x0207
	for(uint8_t i = 0; i < 8; i++)
	{
		TestBuffer[0] = 0x02;
		TestBuffer[1] = i;
		eeprom->Write(MemOffset, TestBuffer, 2);
		MemOffset += 8;
	}

	// Output Select 6
	TestBuffer[0] = 0x03;
	TestBuffer[1] = 0x06;
	eeprom->Write(MemOffset, TestBuffer, 2);
	MemOffset += 8;

	// Output Select 7
	TestBuffer[0] = 0x03;
	TestBuffer[1] = 0x07;
	eeprom->Write(MemOffset, TestBuffer, 2);
	MemOffset += 8;

	// All Output Mute Parameters 0x0400-0x0407
	for(uint8_t i = 0; i < 8; i++)
	{
		TestBuffer[0] = 0x04;
		TestBuffer[1] = i;
		eeprom->Write(MemOffset, TestBuffer, 2);
		MemOffset += 8;
	}

	///// END OF 3WAY DATA
}

void WriteParameterToEEPROM(EEPROM *eeprom, uint32_t Address, uint16_t ID, const char *name, uint16_t RamLocation, uint32_t ScaledMax, uint32_t ScaledMin, uint16_t CurrentValue, uint16_t MaxValue, bool SafeLoad)
{
	uint8_t TestBuffer[64];
	// ID
	TestBuffer[0] = (ID >> 8) & 0xFF;
	TestBuffer[1] = ID & 0xFF;

	// Name
	uint8_t i;
	for(i = 0; i <= 30; i++)
	{
		TestBuffer[2 + i] = *(name+i);
		if(TestBuffer[2+i] == 0)
			break;
	}

	// RAM Location
	TestBuffer[32] = (RamLocation >> 8) & 0xFF;
	TestBuffer[33] = RamLocation & 0xFF;
	// Scaled Max Value
	TestBuffer[34] = (ScaledMax >> 24) & 0xFF;
	TestBuffer[35] = (ScaledMax >> 16) & 0xFF;
	TestBuffer[36] = (ScaledMax >> 8) & 0xFF;
	TestBuffer[37] = ScaledMax & 0xFF;
	// Scaled Min Value
	TestBuffer[38] = (ScaledMin >> 24) & 0xFF;
	TestBuffer[39] = (ScaledMin >> 16) & 0xFF;
	TestBuffer[40] = (ScaledMin >> 8) & 0xFF;
	TestBuffer[41] = ScaledMin & 0xFF;
	// Control Current Value
	TestBuffer[42] = (CurrentValue >> 8) & 0xFF;
	TestBuffer[43] = CurrentValue & 0xFF;
	// Control Max Value
	TestBuffer[44] = (MaxValue >> 8) & 0xFF;
	TestBuffer[45] = MaxValue & 0xFF;
	// SafeLoad
	TestBuffer[46] = SafeLoad ? 0x01 : 0x00;

	eeprom->Write(Address, TestBuffer, 47);
}

void HandleButtonLongPress(uint8_t buttonIndex)
{
	if(buttonIndex < 8)
	{
		// Set parameter control to the output volume of the channel
		parameterControl->SetCurrentParameter(0x0200 + buttonIndex);

		// Get the value of the volume
		uint16_t encoderValue = parameterControl->GetCurrent();
		uint16_t encoderMax = parameterControl->GetCurrentMax();
	    // Set the display encoder value to the current one...
		//SendNewEncoderValueToDisplay((uint8_t)(encoderValue & 0xFF));
		SendNewEncoderValueToDisplay((uint8_t)(encoderValue & 0xFF), (uint8_t)(encoderMax & 0xFF));
	}
	// Long press on the encoder button
	else if(buttonIndex == 8)
	{
		// Set parameter control to the master input volume 1 (for now)
		parameterControl->SetCurrentParameter(0x0100);

		// Get the value of the current parameter
		uint16_t encoderValue = parameterControl->GetCurrent();
		// Get the max value of the current parameter
		uint16_t encoderMax = parameterControl->GetCurrentMax();

	    // Set the display encoder value to the current one...
		//SendNewEncoderValueToDisplay((uint8_t)(encoderValue & 0xFF));
		SendNewEncoderValueToDisplay((uint8_t)(encoderValue & 0xFF), (uint8_t)(encoderMax & 0xFF));
	}

	// Long press on one of the preset buttons
	else if(buttonIndex > 8 && buttonIndex < 12)
	{
		// Save the current state as the preset that was pressed
		// NOTE: Preset is 1 indexed, so valid values are 1,2 and 3!
		parameterControl->SaveCurrentAsPreset(buttonIndex - 8);

		// Let the display update the current selected preset
		SendNewPresetSelectionValueToDisplay(buttonIndex - 8);


		char* string;
		asprintf(&string, "SAVED PRESET %d\n", buttonIndex - 8);
		SendDisplayTextOnce(string);
		free(string);
	}
}

void SendDisplayTextOnce(char* displayText)
{
	uint8_t length = strlen(displayText);

	// We do not send strings larger than 30 characters!
	if(length > 30)
		length = 30;

	uint8_t Buffer[31];
	Buffer[0] = 0x21; // Set text to display
	strcpy(Buffer[1], displayText);

    displayUsart->TrySendMessage(Buffer, displayText+1);

    // Now setup a message to set display mode to text....
	Buffer[0] = 0x10; // set display mode
	Buffer[1] = 2; // text 'once' mode
	displayUsart->TrySendMessage(Buffer, 2);
}

void HandleButtonShortPress(uint8_t buttonIndex)
{
	// If there is a short press on any of the channel buttons..
	if(buttonIndex < 8)
	{
		// Get the current value of the mute state for that channel....
		uint8_t tmp = parameterControl->GetParameterValue(PARAMETERTYPE_MUTE_OUT_0 + buttonIndex);
		// And write back the inverted value...
		parameterControl->SetParameterValue(PARAMETERTYPE_MUTE_OUT_0 + buttonIndex, ((tmp + 1) % 2));

		// We changed something that is part of the current selected preset, So deselect all
		SendNewPresetSelectionValueToDisplay(0);
	}
	// short press on the encoder button
	else if(buttonIndex == 8)
	{
		// No function for now...
	}
	// Short press on one of the Preset buttons..
	else if(buttonIndex > 8 && buttonIndex < 12)
	{
		// Load the values of that preset
		// NOTE: Preset is 1 indexed, so valid values are 1,2 and 3!
		parameterControl->LoadPreset(buttonIndex - 8);

		SendNewPresetSelectionValueToDisplay(buttonIndex - 8);

		// Get the value of the current (UPDATED) parameter
		uint16_t encoderValue = parameterControl->GetCurrent();
		uint16_t encoderMax = parameterControl->GetCurrentMax();
	    // Set the display encoder value to the current one...
		//SendNewEncoderValueToDisplay((uint8_t)(encoderValue & 0xFF));
		SendNewEncoderValueToDisplay((uint8_t)(encoderValue & 0xFF), (uint8_t)(encoderMax & 0xFF));
	}
}

void HandleButtonPresses(uint32_t states)
{
	uint8_t index = 0;
	for(index = 0; index < 12; index++)
	{
		if((states >> index) & 0x0001)
		{
			HandleButtonLongPress(index);
		}
		if((states >> (index + 16)) & 0x0001)
		{
			HandleButtonShortPress(index);
		}
	}
}

void HandleDisplayComm()
{
	uint8_t buf[64];
	uint8_t receivedLength = 0;
	if(displayUsart->TryGetMessage(buf, 64, &receivedLength))
	{
		if(buf[0] == 0x31 && receivedLength == 2)
		{
			encoderValue = buf[1];
		}

		else if(buf[0] == 0x10 && receivedLength == 5)
		{
			buttonStates = buf[1] << 24 | buf[2] << 16 | buf[3] << 8 | buf[4];
		}
	}
}
void SendNewEncoderValueToDisplay(uint8_t newValue, uint8_t newMax)
{
	uint8_t Buffer[4];
	Buffer[0] = 0x31;
	Buffer[1] = newValue;
	Buffer[2] = 0; // For now we always use 0 as the minimum value for the encoder...
	Buffer[3] = newMax;

	displayUsart->TrySendMessage(Buffer, 4);
}

///
/// Preset selection is 1 based, so valid values are 1, 2 and 3
/// Use 0 to deselect all presets
void SendNewPresetSelectionValueToDisplay(uint8_t PresetSelection)
{
	uint8_t Buffer[2];
	Buffer[0] = 0x41;
	Buffer[1] = PresetSelection;

	displayUsart->TrySendMessage(Buffer, 2);
}


void SetupTimer(void)
{
	ctimer_config_t config;
	CTIMER_GetDefaultConfig(&config);
	//config.prescale = 12000;
	// Divide by 48000 to get 1 ms ticks
	config.prescale = 48000;
	CTIMER_Init(CTIMER0, &config);

	CTIMER_StartTimer(CTIMER0);
}

uint32_t GetTimerTick(void)
{
	return CTIMER_GetTimerCountValue(CTIMER0);
}

void MiliSecondDelayBlocking(uint16_t msDelay)
{
    uint32_t tick = GetTimerTick();

    // Wait for the timer to have ticked the needed ms away
    while(GetTimerTick() - tick < msDelay){;}
}

/*
 * @brief   Application entry point.
 */
int main(void) {
  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
	BOARD_InitDebugConsole();


	 // reset FLEXCOMM
	RESET_PeripheralReset(kFC0_RST_SHIFT_RSTn);
	RESET_PeripheralReset(kFC1_RST_SHIFT_RSTn);
	RESET_PeripheralReset(kFC7_RST_SHIFT_RSTn);

    //displayMaster = new DisplayMaster(I2C7, DISPLAY_SLAVE_ADDRESS);
    displayUsart = new USART(USART7, 48000000);

	// Setup the timer for 1 ms ticks
	SetupTimer();

    // Temporary buffer to hold the config bytes
    uint8_t BigAssBuffer[5120] = {0};

    // MasterConfig for the spi communication with the eeprom
    spi_master_config_t masterConfig;

    // Get the spi config for the eeprom communication
    SPI_GetEEPROMConfig(&masterConfig);

    // Create the spi class
    spi_fc0 = new SPI(SPI0, &masterConfig);

    // create an instance of the eeprom class
    eeprom = new EEPROM(spi_fc0, kSPI_Ssel0);

    // Load some test date in the eeprom for now to test the rest of the classes
    //LoadTestDataToEEPROM(eeprom);

    eepromControl = new EEPROMControl(eeprom);

    // TESTING WITH THE I2C TIMEOUT....
    // We should set the I2C wait timeout in the header file...
    //I2C_WAIT_TIMEOUT = 0x00100000;



    // We should move this to some function for cleanness....
    ConfigHeader confHeader = eepromControl->GetConfigHeader(eepromControl->GetDefaultConfig());
    uint8_t headerTextLength = 0;
	uint8_t Buffer[32];
	Buffer[0] = 0x21; // Set text to display

    for(uint8_t i = 0; i < 28; i++)
    {
    	// Copy the text to the sending buffer
    	Buffer[i+1] = confHeader.ConfigName[i];
    	// If this is the terminating 0...
    	if(confHeader.ConfigName[i] == 0)
		{
    		// Set the text length
    		headerTextLength = i;
    		// and exit the loop
    		break;
		}

    }
    displayUsart->TrySendMessage(Buffer, headerTextLength+1);
    // Now setup a message to set display mode to text....
	Buffer[0] = 0x10; // set display mode
	Buffer[1] = 2; // text 'once' mode
	displayUsart->TrySendMessage(Buffer, 2);






    // create an instance of the ADAU1401 class and inject the i2c flexxcomm base we want to use
    adau1401 = new ADAU1401(I2C1, ADAU1401_SLAVE_ADDRESS);


    // Reset the ADAU for 200 ms
    adau1401->SetReset(true);
    MiliSecondDelayBlocking(200);
    adau1401->SetReset(false);




    parameterControl = new ParameterControl(eepromControl, adau1401);

    parameterControl->LoadParameters();


    // Disable the DSP core to prevent sound glitches during load
    adau1401->DisableDSPCore();


    // Get the program ram from the eeprom control
    uint16_t readLength = eepromControl->GetConfigProgramRAM(BigAssBuffer, 5120);

    // Load the program RAM to the ADAU1401
    adau1401->LoadProgramRAM(BigAssBuffer, readLength);


    // Get the parameter ram from the eeprom control
    readLength = eepromControl->GetConfigParameterRAM(BigAssBuffer, 5120);

    // Load the parameter RAM to the ADAU1401
    adau1401->LoadParameterRAM(BigAssBuffer, readLength);

    // Get the hardware registers from the eeprom control
    readLength = eepromControl->GetConfigHardwareRegisters(BigAssBuffer, 5120);

    // Load the hardware registers to the ADAU1401
    adau1401->LoadHardwareRegisters(BigAssBuffer, readLength);


    // Enable the DSP core to activate the device
    adau1401->EnableDSPCore();


    uint8_t lastSelectedPreset = eepromControl->GetLastSelectedPreset();
    if(lastSelectedPreset > 0 && lastSelectedPreset < 4)
    	parameterControl->LoadPreset(lastSelectedPreset);

    SendNewPresetSelectionValueToDisplay(lastSelectedPreset);

    // Set the main volume input channel 1 as the current parameter
    parameterControl->SetCurrentParameter(0x0100);
    // Get and store the current value locally
    encoderValue = parameterControl->GetCurrent();
    uint16_t encoderMax = parameterControl->GetCurrentMax();
    // Have an 'old' value copy to detect changes
    uint16_t oldEncoderValue = encoderValue;

    // Set the display encoder value to the current one...
    //displayMaster->SetEncoderValue((uint8_t)(encoderValue & 0xFF));
    //SendNewEncoderValueToDisplay((uint8_t)(encoderValue & 0xFF));
    SendNewEncoderValueToDisplay((uint8_t)(encoderValue & 0xFF), (uint8_t)(encoderMax & 0xFF));


    while(1)
    {
    	// Try to read data from the Display, if there is anything available
    	HandleDisplayComm();

        // Did the encodervalue changed in the last iteration?
        if(encoderValue != oldEncoderValue)
        {
        	parameterControl->SetCurrent(encoderValue);
        	// Get the parameter id we just changed
        	uint16_t curParamId = parameterControl->GetCurrentParameter();
        	// Check if it is any parameter other than the master volumes
        	if (curParamId != PARAMETERTYPE_MASTERVOLUME_IN_0 &&
        		curParamId != PARAMETERTYPE_MASTERVOLUME_IN_1)
        	{
        		// We changed something else than the master volume, Deselect the current selected preset (even if there is non selected)
        		SendNewPresetSelectionValueToDisplay(0);
        	}
        	oldEncoderValue = encoderValue;
        }

        // Did the button values change?
        if(buttonStates != 0)
        {
        	HandleButtonPresses(buttonStates);
        	buttonStates = 0;
        }
    }
    return 0 ;
}
