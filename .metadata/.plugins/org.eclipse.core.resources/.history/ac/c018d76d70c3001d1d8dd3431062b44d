 /*
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    DisplayControl.cpp
 * @brief   Application entry point.
 */

#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "LPC54113.h"
#include "fsl_debug_console.h"
/* TODO: insert other include files here. */
#include "Display.h"
#include "fsl_inputmux.h"
#include "fsl_gpio.h"
#include "fsl_pint.h"
#include "fsl_ctimer.h"

#include "USART.h"
#include "I2C.h"
#include "LEDManager.h"
#include "Definitions.h"
//#include "LEDs.h"
/* TODO: insert other definitions and declarations here. */

// The states of the short presses detected before they are sent to the master
volatile static uint16_t ButtonShortPressStates = 0x0000;
// The states of the long presses detected before they are sent to the master
volatile static uint16_t ButtonLongPressStates = 0x0000;
// The internal used time stamps for each button's pressed time
volatile static uint32_t ButtonDownTimeStamps[12];
// Indicates the long press for the corresponding button is already handled (needed when the button is still pressed after serviced)
volatile static bool ButtonLongPressHandled[12];

// The previous state of the button port read, set to FFF to indicate all idle at start
volatile static uint32_t previous_button_port_state = 0x00000FFF;

// Pointer to the Master USART Communication class
USART *MasterUsart;
// Pointer to the LED Manager class
LEDManager *LedManager;


 // The current value for the encoder
volatile static int8_t CurrentEncoderValue = 0;
// The current minimum value for the encoder
volatile static int8_t EncoderMinValue = 0;
// The current maximum value for the encoder
volatile static int8_t EncoderMaxValue = 100;
// The change made to the encoder value by the user since the last update
volatile static int8_t EncoderValueChange = 0;


// The current display mode
volatile static display_mode CurrentDisplayMode = displaymode_encoder;//displaymode_text;
// The buffer with ASCII characters to display in text mode
static uint8_t DisplayTextBuffer[32] = {0};
// The current length of the text in the DisplayTextBuffer
static uint8_t DisplayTextLength = 0;
// The delay between scrolling the text one place in milliseconds
static uint16_t DisplayTextScrollDelay = 250;

void pint_intr_callback(pint_pin_int_t pintr, uint32_t pmatch_status);
void Button_ShortPressed(uint8_t buttonIndex);
void Button_LongPressed(uint8_t buttonIndex);
void ReadInputs(void);
void SendNewEncoderValue(uint8_t value);
void SendNewButtonsValue(uint32_t value);
void SetupTimer(void);
uint32_t GetTimerTick(void);
void ReceiveMasterCommMessages(void);


// Callback for the interrupts
// pint0 = Incremental Encoder
void pint_intr_callback(pint_pin_int_t pintr, uint32_t pmatch_status)
{
	/*
	uint32_t port_state = GPIO_PortRead(GPIO, 1);

	if(pintr == 0)
	{
		if(((port_state >> 16) & 0x01) == ((port_state >> 17) & 0x01))
			EncoderValueChange++;
		else
			EncoderValueChange--;
	}
*/

	// NEXT BLOCK IS THE NORMAL CODE TO USE WHEN THE ENCODER IS CONNECTED DIRECTLY!
	// Get the channel B Level
	uint32_t ChannelBLevel = GPIO_PinRead(GPIO, 1, 16);
	if(pintr == 0)
	{
		if(ChannelBLevel == 1)
			EncoderValueChange++;
		else
			EncoderValueChange--;
	}


	// THIS IS FOR TESTING WITH THE PEDLE KEYBOARD AS ENCODER INPUT
	/*
	if(pintr == 0)
		EncoderValueChange++;
	else if(pintr == 1)
		EncoderValueChange--;
*/
}

void Button_ShortPressed(uint8_t buttonIndex)
{
	// set the corresponding bit in the buttonStateRegister
	ButtonShortPressStates |= (1 << buttonIndex);
}
void Button_LongPressed(uint8_t buttonIndex)
{
	// set the corresponding bit in the buttonStateRegister
	ButtonLongPressStates |= (1 << buttonIndex);
}

void ReadInputs(void)
{
	uint32_t port_state = GPIO_PortRead(GPIO, 0);
	uint32_t nowTime = GetTimerTick();
	uint8_t i;

	for(i = 0; i < 12; i++)
	{
		// state change from released to pressed...
		if((previous_button_port_state & (1 << i)) != 0 && (port_state & (1 << i)) == 0)
		{
			ButtonDownTimeStamps[i] = nowTime;
		}

		// If still pressed, check the time....
		else if((previous_button_port_state & (1 << i)) == 0 && (port_state & (1 << i)) == 0)
		{
			// If not handled yet for this button AND time is more than +- 2 seconds....
			if(!ButtonLongPressHandled[i] && nowTime - ButtonDownTimeStamps[i] >= 2000)
			{
				// Set the corresponding button long press flag
				Button_LongPressed(i);

				// Set the long-press-handled flag for this button
				ButtonLongPressHandled[i] = true;
			}
		}

		// state change from pressed to released...
		else if((previous_button_port_state & (1 << i)) == 0 && (port_state & (1 << i)) != 0)
		{
			// Only handle the short presses here, because we already fire the long press when longer than 2 seconds down
			if(nowTime - ButtonDownTimeStamps[i] < 2000)
				Button_ShortPressed(i);

			// Clear the long-press-handled flag for this button
			ButtonLongPressHandled[i] = false;
		}
	}

	// Set the read port state as the previous port state
	previous_button_port_state = port_state;
}

void SendNewEncoderValue(uint8_t value)
{
	uint8_t Buffer[2];
	Buffer[0] = 0x31;
	Buffer[1] = value;

	MasterUsart->TrySendMessage(Buffer, 2);
}

void SendNewButtonsValue(uint32_t value)
{
	uint8_t Buffer[5];
	Buffer[0] = 0x10;
	Buffer[1] = (value >> 24) & 0xFF;
	Buffer[2] = (value >> 16) & 0xFF;
	Buffer[3] = (value >> 8) & 0xFF;
	Buffer[4] = value & 0xFF;

	MasterUsart->TrySendMessage(Buffer, 5);
}

void SetupTimer(void)
{
	ctimer_config_t config;
	CTIMER_GetDefaultConfig(&config);
	//config.prescale = 12000;
	// Divide by 48000 to get 1 ms ticks
	config.prescale = 48000;
	CTIMER_Init(CTIMER0, &config);

	CTIMER_StartTimer(CTIMER0);
}

uint32_t GetTimerTick(void)
{
	return CTIMER_GetTimerCountValue(CTIMER0);
}

void ReceiveMasterCommMessages(void)
{
	uint8_t ReceiveBuffer[64];
	uint8_t ReceivedLength = 0;

	// If we did not receive a new valid message...
	if(!MasterUsart->TryGetMessage(ReceiveBuffer, 64, &ReceivedLength))
		// ..nothing to do here
		return;

	// We need at least two bytes (Command ID and 1 Parameter)
	if(ReceivedLength < 2)
		return;

	// Switch on the Command ID
	switch(ReceiveBuffer[0])
	{
		// Set Display Mode
		case 0x10:
			// Display the encoder value
			if(ReceiveBuffer[1] == displaymode_encoder)
			{
				CurrentDisplayMode = displaymode_encoder;
			}
			// Display the set text as long as we are in this mode
			else if (ReceiveBuffer[1] == displaymode_text)
			{
				CurrentDisplayMode = displaymode_text;
			}
			// Display the set text once (scroll if needed) and then return to encoder display mode
			else if(ReceiveBuffer[1] == displaymode_text_once)
			{
				CurrentDisplayMode = displaymode_text_once;
			}
			break;

		// Set text to display
		case 0x21:

			// We need at least 1 character and a max of 31 (we need the terminating 0)
			if(ReceivedLength < 2 || ReceivedLength > 32)
				break;
			uint8_t i;
			for(i = 0; i < ReceivedLength - 1; i++)
				DisplayTextBuffer[i] = ReceiveBuffer[i + 1];
			// Terminate the string with a 0
			DisplayTextBuffer[i] = 0;

			DisplayTextLength = i;

			break;

		// Set the text scroll delay
		case 0x22:

			// We must have 2  bytes for the 16 bit value, so a length of 3
			if(ReceivedLength != 3)
				break;

			DisplayTextScrollDelay = ReceiveBuffer[1] * 256 + ReceiveBuffer[2];

			break;

		// Set new encoder values (current, min, max)
		case 0x31:
			// We must have 3 parameters, so a length of 4
			if(ReceivedLength != 4)
				break;

			// Set the new encoder values
			CurrentEncoderValue = ReceiveBuffer[1];
			EncoderMinValue = ReceiveBuffer[2];
			EncoderMaxValue = ReceiveBuffer[3];

			break;

		// Set preset led value
		case 0x41:

			// Pass the selection through to the led manager
			LedManager->SetPresetLed(ReceiveBuffer[1]);

			break;

		// Set the master volume led values
		case 0x42:

			// We must have 2 parameters, so a length of 3
			if(ReceivedLength != 3)
				break;

			LedManager->SetMasterVolumeLeds(ReceiveBuffer[1], ReceiveBuffer[2]);

			break;

		// Set the led values for a specific channel
		case 0x43:

			// If we have 2 parameters (length = 3)
			if(ReceivedLength == 3)
			{
				// We have a channel led state value for all leds, without a bit-mask
				// Pass the channel/zone and the state to the led manager
				LedManager->SetZoneState(ReceiveBuffer[1], (zone_led_mask)ReceiveBuffer[2]);
			}
			// If we have 3 parameters (length = 4)
			else if(ReceivedLength == 4)
			{
				// We have a channel led state value with a led mask for the leds to change!
				// Pass the channel/zone, the state and the mask to the led manager
				LedManager->SetZoneState(ReceiveBuffer[1], (zone_led_mask)ReceiveBuffer[2], (zone_led_mask)ReceiveBuffer[3]);
			}

			break;
	}

}


void TempFunctionSetAllZoneStates(zone_led_mask state)
{
	LedManager->SetZoneState(0, state);
	LedManager->SetZoneState(1, state);
	LedManager->SetZoneState(2, state);
	LedManager->SetZoneState(3, state);
	LedManager->SetZoneState(4, state);
	LedManager->SetZoneState(5, state);
	LedManager->SetZoneState(6, state);
	LedManager->SetZoneState(7, state);
}

void DisplayTextHandler()
{
	static uint32_t previousTextUpdateTick = 0;
	static int8_t TextScrollIndex = 0;
	static bool StartTextDisplay = true;

	// We are starting a new cycle of the text display
	if(StartTextDisplay)
	{
		StartTextDisplay = false;
		// we add 3 times the delay to have the start of the string 4 time the delay time in the screen
		previousTextUpdateTick = GetTimerTick() + 3*DisplayTextScrollDelay;
		TextScrollIndex = 0;
	}
	WriteToDisplay(DisplayTextBuffer, TextScrollIndex);

	if(GetTimerTick() >= previousTextUpdateTick + DisplayTextScrollDelay)
	{
		previousTextUpdateTick = GetTimerTick();
		TextScrollIndex++;
		if(TextScrollIndex >= DisplayTextLength)
		{
			if(CurrentDisplayMode == displaymode_text_once)
			{
				// return to displaying the encoder value
				CurrentDisplayMode = displaymode_encoder;
				StartTextDisplay = true;
				return;
			}
			TextScrollIndex = -4;
		}
		if(TextScrollIndex == 0)
			StartTextDisplay = true;
	}

}


/*
 * @brief   Application entry point.
 */
int main(void) {
  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
	BOARD_InitDebugConsole();

	 // reset FLEXCOMM
	RESET_PeripheralReset(kFC1_RST_SHIFT_RSTn);
	RESET_PeripheralReset(kFC4_RST_SHIFT_RSTn);
	RESET_PeripheralReset(kFC7_RST_SHIFT_RSTn);

	MasterUsart = new USART(USART7, 48000000);

	//Init_LEDs(new I2C(I2C4));
	LedManager = new LEDManager(I2C4);

	//Display_I2C = new I2C(I2C1);
	Init_Display(new I2C(I2C1));

	// Make sure we set all long press handled flags to false
	for(uint8_t i = 0; i < 12; i++)
		ButtonLongPressHandled[i] = false;


	// Setup the timer to be able to use time intervals
	SetupTimer();


	// For the GPIO use
	CLOCK_EnableClock(kCLOCK_Gpio0);
	GPIO_PortInit(GPIO, 0);
	GPIO_PortInit(GPIO, 1);


	INPUTMUX_Init(INPUTMUX);

	INPUTMUX_AttachSignal(INPUTMUX, kPINT_PinInt0, kINPUTMUX_GpioPort1Pin17ToPintsel);

	// Added for testing with the Pedle Keyboard input
	INPUTMUX_AttachSignal(INPUTMUX, kPINT_PinInt1, kINPUTMUX_GpioPort1Pin16ToPintsel);

	/* Turn off clock to input mux to save power. Clock is only needed to make changes */
	INPUTMUX_Deinit(INPUTMUX);


	/* Initialize PINT */
	PINT_Init(PINT);

	/* Setup Pin Interrupt 0 for rising edge */
	PINT_PinInterruptConfig(PINT, kPINT_PinInt0, kPINT_PinIntEnableRiseEdge, pint_intr_callback);

	// Added for testing with the Pedle Keyboard input
	//PINT_PinInterruptConfig(PINT, kPINT_PinInt1, kPINT_PinIntEnableRiseEdge, pint_intr_callback);

	/* Enable callbacks for PINT */
	PINT_EnableCallback(PINT);

	//int8_t PreviousEncoderValue = CurrentEncoderValue;
	int8_t PreviousEncoderValue = 0;

	//uint32_t timerTick = GetTimerTick();

	while(1)
	{
		//uint32_t newTimerTick = GetTimerTick();

		//printf("%i\r\n", newTimerTick - timerTick);


		ReceiveMasterCommMessages();

		ReadInputs();

		if(ButtonShortPressStates != 0 || ButtonLongPressStates != 0)
		{
			SendNewButtonsValue(ButtonShortPressStates << 16 | ButtonLongPressStates);
			ButtonShortPressStates = 0;
			ButtonLongPressStates = 0;
		}


		int16_t tempNewEncoderValue = CurrentEncoderValue + EncoderValueChange;
		EncoderValueChange = 0;

		if(tempNewEncoderValue > EncoderMaxValue)
			CurrentEncoderValue = EncoderMaxValue;
		else if(tempNewEncoderValue < EncoderMinValue)
			CurrentEncoderValue = EncoderMinValue;
		else
			CurrentEncoderValue = tempNewEncoderValue;



 		if(CurrentEncoderValue != PreviousEncoderValue)
		{
 			SendNewEncoderValue(CurrentEncoderValue);
			PreviousEncoderValue = CurrentEncoderValue;

			// If we are in the mode that displays the current encoder value...
	 		if(CurrentDisplayMode == displaymode_encoder)
	 			// .. we write the new version to the display
	 			WriteNumberValueToDisplay(CurrentEncoderValue);

		}


 		//if(CurrentDisplayMode == displaymode_encoder)
 			//WriteNumberValueToDisplay(CurrentEncoderValue);
 		//else
 		// If we are not in the display encoder mode...
 		if(CurrentDisplayMode != displaymode_encoder)
 		{
 			// We need to handle the text displaying fuction....
 			DisplayTextHandler();
 		}
	}
}
