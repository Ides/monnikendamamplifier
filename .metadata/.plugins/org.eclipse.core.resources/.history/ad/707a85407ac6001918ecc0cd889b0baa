/**
 * @file    eeprom.c
 * @brief   EEPROM communication file for the on-board 2Mbit chip
 */

/* This is a template for board specific configuration created by MCUXpresso IDE Project Wizard.*/

#include <eepromControl.h>
#include <stdio.h>


EEPROMControl::EEPROMControl(EEPROM *eeprom)
{
	_eeprom = eeprom;
	ReadTOC();
	SelectCurrentConfig(DefaultConfig);
}

EEPROMControl::~EEPROMControl()
{
	ConfigHeaders.clear();
}



// Sets the given config index as the current and Pre-loads all the memory offsets and the parameter count
void EEPROMControl::SelectCurrentConfig(uint8_t configIndex)
{
	uint16_t size;

	// Set the current config to the given index
	CurrentConfig = configIndex;

	// Get the Config Base address, This is also the start of the Program RAM
	CurrentConfigProgramRAMOffset = GetConfigBaseAddress(CurrentConfig);

	// First get the size of the Program RAM
	_eeprom->Read(CurrentConfigProgramRAMOffset, &size);

	// Calculate the offset to the base of the ParameterRAM
	CurrentConfigParameterRAMOffset = CurrentConfigProgramRAMOffset + 2 + size;

	// Get the size of the Parameter RAM
	_eeprom->Read(CurrentConfigParameterRAMOffset, &size);

	// Calculate the offset to the base of the Hardware Registers
	CurrentConfigHardwareRegisterOffset = CurrentConfigParameterRAMOffset + 2 + size;

	// Add the size of the hardware register block to get the start of the Parameters
	CurrentConfigParameterOffset = CurrentConfigHardwareRegisterOffset + 24;

	// Get the number of Parameters for this Config
	_eeprom->Read(CurrentConfigParameterOffset, &CurrentConfigParameterCount, 1);

	// Calculate the Preset offset
	CurrentConfigPresetOffset = CurrentConfigParameterOffset + 1 + CurrentConfigParameterCount*47;
}

// Reads the Program RAM bytes for the Current Config
// Returns the actual bytes read
uint16_t EEPROMControl::GetConfigProgramRAM(uint8_t *Buffer, uint16_t bufLength)
{
	uint16_t size;
	_eeprom->Read(CurrentConfigProgramRAMOffset, &size);

	// Make sure we have enough room in the buffer provided
	if(size > bufLength)
		return 0;

	// size should be a multiple of 5 for the Program RAM
	if(size % 5 != 0)
		return 0;

	_eeprom->Read(CurrentConfigProgramRAMOffset + 2, Buffer, size);

	return size;
}

// Reads the Parameter RAM bytes for the Current Config
// Returns the actual bytes read
uint16_t EEPROMControl::GetConfigParameterRAM(uint8_t *Buffer, uint16_t bufLength)
{
	uint16_t size;
	// Get the size of the Parameter RAM
	_eeprom->Read(CurrentConfigParameterRAMOffset, &size);

	// Make sure we have enough room in the buffer provided
	if(size > bufLength)
		return 0;

	// size should be a multiple of 4 for the Parameter RAM
	if(size % 4 != 0)
		return 0;


	_eeprom->Read(CurrentConfigParameterRAMOffset + 2, Buffer, size);

	return size;
}

// Reads the Hardware Register bytes for the Current Config
// Returns the actual bytes read (should be 24 always
uint16_t EEPROMControl::GetConfigHardwareRegisters(uint8_t *Buffer, uint16_t bufLength)
{
	// Make sure we have enough room in the buffer provided
	if(bufLength < 24)
		return 0;

	// Read the hardware registers (always 24 bytes)
	_eeprom->Read(CurrentConfigHardwareRegisterOffset, Buffer, 24);

	return 24;
}

// Gets the ParameterDecsription structure for the Given index of the current config
ParameterDescriptor EEPROMControl::GetParameterDescriptor(uint8_t Index)
{
	ParameterDescriptor desc;
	if(Index >= CurrentConfigParameterCount)
	{
		desc.parameter_id = 0;
		return desc;
	}

	uint8_t ParameterBuffer[47];
	_eeprom->Read(CurrentConfigParameterOffset + 1 + Index*47, ParameterBuffer, 47);

	desc.parameter_id = (ParameterBuffer[0] << 8) + ParameterBuffer[1];
	memcpy(&desc.parameterName, &ParameterBuffer[2], 30);
	desc.memoryLocation = (ParameterBuffer[32] << 8) + ParameterBuffer[33];
	desc.scaledMaxValue = (ParameterBuffer[34] << 24) + (ParameterBuffer[35] << 16) + (ParameterBuffer[36] << 8) + ParameterBuffer[37];
	desc.scaledMinValue = (ParameterBuffer[38] << 24) + (ParameterBuffer[39] << 16) + (ParameterBuffer[40] << 8) + ParameterBuffer[41];
	desc.currentValue = (ParameterBuffer[43] << 8) + ParameterBuffer[43];
	desc.maxValue = (ParameterBuffer[44] << 8) + ParameterBuffer[45];
	desc.useSafeLoad = (ParameterBuffer[46] == 0x01) ? true : false;

	return desc;
}


// Reads the table of contents of the EEPROM and lists the config headers (name and offset)
void EEPROMControl::ReadTOC()
{
	uint8_t Buf[32];
	uint16_t i;
	_eeprom->Read(0x00000000, Buf, 8);

	// Check for the 'magic' string in the first four bytes
	if(Buf[0] != 'I' || Buf[1] != 'D' || Buf[2] != 'E' || Buf[3] != 'S')
	{
		ConfigCount = 0;
		return;
	}

	EEPROMVersion = Buf[4];
	ConfigCount = Buf[5];
	DefaultConfig = Buf[6];

	uint8_t checkSum = 0;
	for(i = 0; i < 7; i++)
		checkSum += Buf[i];

	if(checkSum != Buf[7])
	{
		ConfigCount = 0;
		return;
	}

	for(i = 0; i < ConfigCount; i++)
	{
		ConfigHeader header;
		_eeprom->Read(0x00000008 + 0x20 * i, (uint8_t*)&header, 0x20);

		//std::string s(reinterpret_cast<char*>(Buf), 28);
		//header.MemoryOffset = (Buf[28] << 24) + (Buf[29] << 16) + (Buf[30] << 8) + Buf[31];
		ConfigHeaders.push_back(header);
	}
}

// Gets the config base address of the given config index
uint32_t EEPROMControl::GetConfigBaseAddress(uint8_t configIndex)
{
	ConfigHeader const & header = ConfigHeaders.at(configIndex);

	return (header.MemoryOffset[0] << 24)
			+ (header.MemoryOffset[1] << 16)
			+ (header.MemoryOffset[2] << 8)
			+ header.MemoryOffset[3];
}
