/*
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    AmpControl.cpp
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "LPC54113.h"
#include "fsl_debug_console.h"
/* TODO: insert other include files here. */

#include "spi.h"
#include "i2c.h"
#include "adau1401.h"
#include "eeprom.h"
#include "parameter.h"
/* TODO: insert other definitions and declarations here. */


#define ADAU1401_SLAVE_ADDRESS	0x34

SPI *spi_fc0;
ADAU1401 *adau1401;
EEPROM *eeprom;
std::list<Parameter> ParameterList;

void SPI_GetEEPROMConfig(spi_master_config_t *config)
{
    assert(NULL != config);

    /* Initializes the configure structure to zero. */
    memset(config, 0, sizeof(*config));

    config->enableLoopback = false;
    config->enableMaster = true;
    config->polarity = kSPI_ClockPolarityActiveHigh;
    config->phase = kSPI_ClockPhaseFirstEdge;
    config->direction = kSPI_MsbFirst;
    config->baudRate_Bps = 5000000;
    config->dataWidth = kSPI_Data8Bits;
    config->sselNum = kSPI_Ssel0;
    config->txWatermark = kSPI_TxFifo0;
    config->rxWatermark = kSPI_RxFifo1;
    config->sselPol = kSPI_SpolActiveAllLow;
    config->delayConfig.preDelay = 0U;
    config->delayConfig.postDelay = 0U;
    config->delayConfig.frameDelay = 0U;
    config->delayConfig.transferDelay = 0U;
}
/*
  		{"Master Volume Input 0", 0x0014, 0x00000800, 0x00000800, 0x03000000, 0, 0xFF, 1 },
		{"Master Volume Input 1", 0x0016, 0x00000800, 0x00000800, 0x03000000, 0, 0xFF, 1 },
		{"Master Volume Output 0", 0x002A, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
		{"Master Volume Output 1", 0x002C, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
		{"Master Volume Output 2", 0x002E, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
		{"Master Volume Output 3", 0x0030, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
		{"Master Volume Output 4", 0x0032, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
		{"Master Volume Output 5", 0x0034, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
		{"Master Volume Output 6", 0x0036, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
		{"Master Volume Output 7", 0x0038, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },

		{"Output Select 0", 0x0022, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },
		{"Output Select 1", 0x0023, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },
		{"Output Select 2", 0x0024, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },
		{"Output Select 3", 0x0025, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },
		{"Output Select 4", 0x0026, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },
		{"Output Select 5", 0x0027, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },
		{"Output Select 6", 0x0028, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },
		{"Output Select 7", 0x0029, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },

		{"Output MUTE 0", 0x003A, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },
		{"Output MUTE 1", 0x003C, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },
		{"Output MUTE 2", 0x003E, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },
		{"Output MUTE 3", 0x0040, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },
		{"Output MUTE 4", 0x0042, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },
		{"Output MUTE 5", 0x0044, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },
		{"Output MUTE 6", 0x0046, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },
		{"Output MUTE 7", 0x0048, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },*/
void InitParameterList()
{
	ParameterDescriptor desc;
	desc.currentValue = 0;
	desc.maxValue = 0x63;
	desc.memoryLocation = 0x0014;
	desc.parameter_id = 0;
	desc.scaledMinValue = 0x00000800;
	desc.scaledMaxValue = 0x03000000;
	desc.useSafeLoad = false;

	ParameterList.push_back(new Parameter(desc));
}

/*
 * @brief   Application entry point.
 */
int main(void) {
  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
	BOARD_InitDebugConsole();


	 // reset FLEXCOMM
	RESET_PeripheralReset(kFC0_RST_SHIFT_RSTn);
	RESET_PeripheralReset(kFC1_RST_SHIFT_RSTn);
	RESET_PeripheralReset(kFC6_RST_SHIFT_RSTn);

    // MasterConfig for the spi communication with the eeprom
    spi_master_config_t masterConfig;

    // Get the spi config for the eeprom communication
    SPI_GetEEPROMConfig(&masterConfig);

    // Create the spi class
    spi_fc0 = new SPI(SPI0, &masterConfig);

    // create an instance of the eeprom class
    eeprom = new EEPROM(spi_fc0, kSPI_Ssel0);

    // create an instance of the ADAU1401 class and inject the i2c flexxcomm base we want to use
    adau1401 = new ADAU1401(I2C1, ADAU1401_SLAVE_ADDRESS);


    // Temporary buffer to hold the config bytes
    uint8_t BigAssBuffer[5120] = {0};

    // Disable the DSP core to prevent sound glitches during load
    adau1401->DisableDSPCore();

    // read the program RAM bytes from the eeprom (for now it is located at an offset of 0x1000)
    eeprom->Read(0x1000, BigAssBuffer, 5120);

    // Load the program RAM to the ADAU1401
    adau1401->LoadProgramRAM(BigAssBuffer, 5120);


    // read the parameter RAM bytes from the eeprom (for now it is located at an offset of 0x5000)
    eeprom->Read(0x5000, BigAssBuffer, 4096);

    // Load the parameter RAM to the ADAU1401
    adau1401->LoadParameterRAM(BigAssBuffer, 4096);


    // read the parameter RAM bytes from the eeprom (for now it is located at an offset of 0x5000)
    eeprom->Read(0x2400, BigAssBuffer, 24);

    // Load the parameter RAM to the ADAU1401
    adau1401->LoadHardwareRegisters(BigAssBuffer, 24);

    // Enable the DSP core to activate the device
    adau1401->EnableDSPCore();


    InitParameterList();

    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */
    while(1) {
        i++ ;
    }
    return 0 ;
}
