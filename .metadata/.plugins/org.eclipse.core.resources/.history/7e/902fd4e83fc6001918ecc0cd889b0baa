/**
 * @file    eeprom.c
 * @brief   EEPROM communication file for the on-board 2Mbit chip
 */

/* This is a template for board specific configuration created by MCUXpresso IDE Project Wizard.*/

#include <parameter.h>
#include <stdint.h>
#include "fsl_debug_console.h"
#include "fsl_gpio.h"


// Constructor
ParameterControl::ParameterControl(EEPROMControl *eepromControl, ADAU1401 *adau)
{
	_adau1401 = adau;
	_eepromControl = eepromControl;

	parameter_id = descriptor->parameter_id;
	memoryLocation = descriptor->memoryLocation;
//	scaledCurrentValue = descriptor->scaledCurrentValue;
	scaledMinValue = descriptor->scaledMinValue;
	scaledMaxValue = descriptor->scaledMaxValue;
	currentValue = descriptor->currentValue;
	maxValue = descriptor->maxValue;

	//ParameterName = descriptor->parameterName;
	useSafeLoad = descriptor->useSafeLoad;

	CalculateScaledValue();
}

Parameter::~Parameter()
{
	_adau1401 = NULL;
}

void Parameter::AddToValue(int16_t valueToAdd)
{
	currentValue += valueToAdd;

	if(currentValue > maxValue)
		currentValue = maxValue;

	CalculateScaledValue();

	UpdateValueToADAU();
}

void Parameter::SetValue(uint16_t value)
{
	if(value > maxValue)
		value = maxValue;

	currentValue = value;

	CalculateScaledValue();

	UpdateValueToADAU();
}
uint16_t Parameter::GetValue()
{
	return currentValue;
}

uint8_t Parameter::GetID()
{
	return parameter_id;
}

void Parameter::Update()
{
	UpdateValueToADAU();
}

void Parameter::CalculateScaledValue()
{
	// Calculate what the lsb value of one 'value' click is
	double x = (scaledMaxValue - scaledMinValue) / maxValue;

	// Multiply by the current value;
	double d = x * currentValue;

	// Add the min value to shift the scaled value
	scaledCurrentValue = scaledMinValue + (uint32_t)d;
}

void Parameter::UpdateValueToADAU()
{
	if(useSafeLoad)
		_adau1401->SetParameterSafe(memoryLocation, scaledCurrentValue);
	else
		_adau1401->SetParameter(memoryLocation, scaledCurrentValue);
}
