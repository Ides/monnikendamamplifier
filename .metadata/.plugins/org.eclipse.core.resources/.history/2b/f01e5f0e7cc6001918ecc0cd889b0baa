#ifndef _EEPROMCONTROL_H_
#define _EEPROMCONTROL_H_

#include <stdint.h>
#include <stdio.h>
#include "fsl_spi.h"
#include "eeprom.h"
#include <vector>
#include <string>

#include "Parameter.h"

struct ConfigHeader
{
	uint8_t ConfigName[28];
	uint8_t MemoryOffset[4];
};

class EEPROMControl
{
private:
	EEPROM *_eeprom;

	uint8_t EEPROMVersion;
	uint8_t ConfigCount;
	uint8_t DefaultConfig;

	// The current config we are reading and writing from/to
	// Will default to the DefaultConfig value
	uint8_t CurrentConfig;
	uint32_t CurrentConfigParameterRAMOffset = 0;
	uint32_t CurrentConfigProgramRAMOffset = 0;
	uint32_t CurrentConfigHardwareRegisterOffset = 0;
	uint32_t CurrentConfigParameterOffset = 0;
	uint32_t CurrentConfigPresetOffset = 0;
	uint8_t CurrentConfigParameterCount = 0;

	std::vector<ConfigHeader> ConfigHeaders;

	void ReadTOC(void);
	uint32_t GetConfigBaseAddress(uint8_t configIndex);
public:
	// Constructor
	EEPROMControl(EEPROM *eeprom);
	// Destructor
	virtual ~EEPROMControl();

	uint8_t GetEEPROMVersion(void) { return EEPROMVersion;}
	uint8_t GetConfigCount(void) { return ConfigCount;}
	uint8_t GetDefaultConfig(void) { return DefaultConfig;}
	uint8_t GetParameterCount(void) {return CurrentConfigParameterCount;}
	void SelectCurrentConfig(uint8_t configIndex);

	uint16_t GetConfigProgramRAM(uint8_t *Buffer, uint16_t bufLength);
	uint16_t GetConfigParameterRAM(uint8_t *Buffer, uint16_t bufLength);
	uint16_t GetConfigHardwareRegisters(uint8_t *Buffer, uint16_t bufLength);

	ParameterDescriptor GetParameterDescriptor(uint8_t Index);
};


#endif /* _EEPROMCONTROL_H_ */
