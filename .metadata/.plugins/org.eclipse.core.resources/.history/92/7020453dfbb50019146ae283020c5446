/*
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    AmpControl.cpp
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "LPC54113.h"
#include "fsl_debug_console.h"
#include <list>
/* TODO: insert other include files here. */

#include "spi.h"
#include "i2c.h"
#include "adau1401.h"
#include "eeprom.h"
#include "eepromControl.h"
#include "parameter.h"


#include "Monnikendam v1.1_ADAU1401.h"
/* TODO: insert other definitions and declarations here. */


#define ADAU1401_SLAVE_ADDRESS	0x34

SPI *spi_fc0;
ADAU1401 *adau1401;
EEPROM *eeprom;
EEPROMControl *eepromControl;
std::list<Parameter> ParameterList;

void SPI_GetEEPROMConfig(spi_master_config_t *config)
{
    assert(NULL != config);

    /* Initializes the configure structure to zero. */
    memset(config, 0, sizeof(*config));

    config->enableLoopback = false;
    config->enableMaster = true;
    config->polarity = kSPI_ClockPolarityActiveHigh;
    config->phase = kSPI_ClockPhaseFirstEdge;
    config->direction = kSPI_MsbFirst;
    config->baudRate_Bps = 5000000;
    config->dataWidth = kSPI_Data8Bits;
    config->sselNum = kSPI_Ssel0;
    config->txWatermark = kSPI_TxFifo0;
    config->rxWatermark = kSPI_RxFifo1;
    config->sselPol = kSPI_SpolActiveAllLow;
    config->delayConfig.preDelay = 0U;
    config->delayConfig.postDelay = 0U;
    config->delayConfig.frameDelay = 0U;
    config->delayConfig.transferDelay = 0U;
}
/*
  		{"Master Volume Input 0", 0x0014, 0x00000800, 0x00000800, 0x03000000, 0, 0xFF, 1 },
		{"Master Volume Input 1", 0x0016, 0x00000800, 0x00000800, 0x03000000, 0, 0xFF, 1 },
		{"Master Volume Output 0", 0x002A, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
		{"Master Volume Output 1", 0x002C, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
		{"Master Volume Output 2", 0x002E, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
		{"Master Volume Output 3", 0x0030, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
		{"Master Volume Output 4", 0x0032, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
		{"Master Volume Output 5", 0x0034, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
		{"Master Volume Output 6", 0x0036, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
		{"Master Volume Output 7", 0x0038, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },

		{"Output Select 0", 0x0022, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },
		{"Output Select 1", 0x0023, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },
		{"Output Select 2", 0x0024, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },
		{"Output Select 3", 0x0025, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },
		{"Output Select 4", 0x0026, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },
		{"Output Select 5", 0x0027, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },
		{"Output Select 6", 0x0028, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },
		{"Output Select 7", 0x0029, 0x00000000, 0x00000000, 0x00000001, 0, 0x01, 1 },

		{"Output MUTE 0", 0x003A, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },
		{"Output MUTE 1", 0x003C, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },
		{"Output MUTE 2", 0x003E, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },
		{"Output MUTE 3", 0x0040, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },
		{"Output MUTE 4", 0x0042, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },
		{"Output MUTE 5", 0x0044, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },
		{"Output MUTE 6", 0x0046, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },
		{"Output MUTE 7", 0x0048, 0x00800000, 0x00000000, 0x00800000, 0, 0x01, 1 },*/
void InitParameterList()
{
	ParameterDescriptor desc;
	desc.currentValue = 0;
	desc.maxValue = 0x63;
	desc.memoryLocation = 0x0014;
	desc.parameter_id = 0;
	desc.scaledMinValue = 0x00000800;
	desc.scaledMaxValue = 0x03000000;
	desc.useSafeLoad = false;

	Parameter p = Parameter(adau1401, &desc);
	ParameterList.push_back(p);
}

void LoadTestDataToEEPROM(EEPROM *eeprom)
{
	uint8_t TestBuffer[64];

	eeprom->Read(0x00, TestBuffer, 32);

	// Magic string
	TestBuffer[0] = 'I';
	TestBuffer[1] = 'D';
	TestBuffer[2] = 'E';
	TestBuffer[3] = 'S';
	// EEPROM VERSIO
	TestBuffer[4] = 1;
	// No of configs
	TestBuffer[5] = 3;
	// Default Config
	TestBuffer[6] = 0;
	// Checksum
	TestBuffer[7] = 0x29;

	eeprom->Write(0x00, TestBuffer, 8);

	TestBuffer[0] = 'T';
	TestBuffer[1] = 'e';
	TestBuffer[2] = 's';
	TestBuffer[3] = 't';
	TestBuffer[4] = '1';
	TestBuffer[5] = 0;
	TestBuffer[28] = 0x00;
	TestBuffer[29] = 0x00;
	TestBuffer[30] = 0x01;
	TestBuffer[31] = 0x00;
	eeprom->Write(0x08, TestBuffer, 32);

	TestBuffer[4] = '2';
	TestBuffer[5] = 0;
	TestBuffer[28] = 0x00;
	TestBuffer[29] = 0x02;
	TestBuffer[30] = 0x34;
	TestBuffer[31] = 0x56;
	eeprom->Write(0x28, TestBuffer, 32);

	TestBuffer[4] = '3';
	TestBuffer[5] = 0;
	TestBuffer[28] = 0x00;
	TestBuffer[29] = 0x03;
	TestBuffer[30] = 0x45;
	TestBuffer[31] = 0x67;
	eeprom->Write(0x48, TestBuffer, 32);

	uint32_t MemOffset = 0x00000100;

	TestBuffer[0] = 0x14;
	TestBuffer[1] = 0x00;
	eeprom->Write(MemOffset, TestBuffer, 2);
	MemOffset += 2;

	eeprom->Write(MemOffset, Program_Data_ADAU1401, 5120);
	MemOffset += 5120;

	TestBuffer[0] = 0x10;
	TestBuffer[1] = 0x00;
	eeprom->Write(0x00001502, TestBuffer, 2);
	MemOffset += 2;

	eeprom->Write(0x00001504, Param_Data_ADAU1401, 4096);
	MemOffset += 4096;

	eeprom->Write(0x00002504, R3_HWCONFIGURATION_ADAU1401_Default, 24);
	MemOffset += 24;

	// No. of Parameters
	TestBuffer[0] = 30;
	eeprom->Write(MemOffset, TestBuffer, 1);
	MemOffset += 1;

//	{"Master Volume Input 0", 0x0014, 0x00000800, 0x00000800, 0x03000000, 0, 0xFF, 1 }
	// Parameter ID
	TestBuffer[0] = 0x00;
	TestBuffer[1] = 0x10;
	// Name
	TestBuffer[2] = 'M';
	TestBuffer[3] = 'V';
	TestBuffer[4] = 'I';
	TestBuffer[5] = '0';
	TestBuffer[6] = 0;
	// RAM Location
	TestBuffer[32] = 0x00;
	TestBuffer[33] = 0x14;
	// Scaled Max Value
	TestBuffer[34] = 0x03;
	TestBuffer[35] = 0x00;
	TestBuffer[36] = 0x00;
	TestBuffer[37] = 0x00;
	// Scaled Min Value
	TestBuffer[38] = 0x00;
	TestBuffer[39] = 0x00;
	TestBuffer[40] = 0x08;
	TestBuffer[41] = 0x00;
	// Control Current Value
	TestBuffer[42] = 0x00;
	TestBuffer[43] = 0x00;
	// Control Max Value
	TestBuffer[44] = 0x00;
	TestBuffer[45] = 0x64;



//	{"Master Volume Input 1", 0x0016, 0x00000800, 0x00000800, 0x03000000, 0, 0xFF, 1 },
//	{"Master Volume Output 0", 0x002A, 0x03000000, 0x00000800, 0x03000000, 0xFF, 0xFF, 1 },
}


/*
 * @brief   Application entry point.
 */
int main(void) {
  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
	BOARD_InitDebugConsole();


	 // reset FLEXCOMM
	RESET_PeripheralReset(kFC0_RST_SHIFT_RSTn);
	RESET_PeripheralReset(kFC1_RST_SHIFT_RSTn);
	RESET_PeripheralReset(kFC6_RST_SHIFT_RSTn);

    // Temporary buffer to hold the config bytes
    uint8_t BigAssBuffer[5120] = {0};

    // MasterConfig for the spi communication with the eeprom
    spi_master_config_t masterConfig;

    // Get the spi config for the eeprom communication
    SPI_GetEEPROMConfig(&masterConfig);

    // Create the spi class
    spi_fc0 = new SPI(SPI0, &masterConfig);

    // create an instance of the eeprom class
    eeprom = new EEPROM(spi_fc0, kSPI_Ssel0);

    // Load some test date in the eeprom for now to test the rest of the classes
    LoadTestDataToEEPROM(eeprom);

    eepromControl = new EEPROMControl(eeprom);


    // create an instance of the ADAU1401 class and inject the i2c flexxcomm base we want to use
    adau1401 = new ADAU1401(I2C1, ADAU1401_SLAVE_ADDRESS);

    // Disable the DSP core to prevent sound glitches during load
    adau1401->DisableDSPCore();


    // Get the program ram from the eeprom control
    uint16_t readLength = eepromControl->GetConfigProgramRAM(BigAssBuffer, 5120);

    // Load the program RAM to the ADAU1401
    adau1401->LoadProgramRAM(BigAssBuffer, readLength);

    // Get the parameter ram from the eeprom control
    readLength = eepromControl->GetConfigParameterRAM(BigAssBuffer, 5120);

    // Load the parameter RAM to the ADAU1401
    adau1401->LoadParameterRAM(BigAssBuffer, readLength);

    // Get the hardware registers from the eeprom control
    readLength = eepromControl->GetConfigHardwareRegisters(BigAssBuffer, 5120);

    // Load the hardware registers to the ADAU1401
    adau1401->LoadHardwareRegisters(BigAssBuffer, readLength);


    // Enable the DSP core to activate the device
    adau1401->EnableDSPCore();


    InitParameterList();

    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */
    while(1) {
        i++ ;
    }
    return 0 ;
}
