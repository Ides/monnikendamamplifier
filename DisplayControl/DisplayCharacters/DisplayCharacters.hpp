#ifndef DISPLAYCHARACTERS_HPP_
#define DISPLAYCHARACTERS_HPP_


/********************************


   ___________________
 _<_________0_________>_
/ \  _      _      _  / \
| | | \    / \    / | | |
| |  \ \   | |   / /  | |
|5|   \6\  |7|  /8/   |1|
| |    \ \ | | / /    | |
| |     \_|\_/|_/     | |
\_/  ______   ______  \_/
 _  <__13__> <___9__>  _
/ \      _  _  _      / \
| |     / |/ \| \     | |
| |    / / | | \ \    | |
|4|   /2/  |1|  \1\   |2|
| |  /1/   |1|   \0\  | |
| | |_/    \_/    \_| | |
\_/___________________\_/    ____
  <_________3_________>     /    \
                            | 14 |
                            \____/
 */

// The bits in the display number definitions match the segment numbers defined in above 'image'


uint16_t DisplayNumbers[10] = {
		0x113F, /* 0 */
		0x0106, /* 1 */
		0x221B, /* 2 */
		0x020F, /* 3 */
		0x2226, /* 4 */
		0x2429, /* 5 */
		0x223D, /* 6 */
		0x0007, /* 7 */
		0x223F, /* 8 */
		0x222F  /* 9 */
};

uint16_t DisplayLetters[26] = {
		0x2237, /* A */
		0x0A8F, /* B */
		0x0039, /* C */
		0x088F, /* D */
		0x2039, /* E */
		0x2031, /* F */
		0x023D, /* G */
		0x2236, /* H */
		0x0889, /* I */
		0x001E, /* J */
		0x2530, /* K */
		0x0038, /* L */
		0x0176, /* M */
		0x0476, /* N */
		0x003F, /* O */
		0x2233, /* P */
		0x043F, /* Q */
		0x2633, /* R */
		0x222D, /* S */
		0x0881, /* T */
		0x003E, /* U */
		0x1130, /* V */
		0x1436, /* W */
		0x1540, /* X */
		0x222E, /* Y */
		0x1109  /* Z */
};

uint16_t DisplaySpecialCharacters[6] = {
		0x2A80, /* + */
		0x2200, /* - */
		0x1100, /* / */
		0x0440, /* \ */
		0x0880, /* | */
		0x3FC0, /* * */
};

#define SpecialCharacter_Plus		0
#define SpecialCharacter_Minus		1
#define SpecialCharacter_Slash		2
#define SpecialCharacter_BSlash		3
#define SpecialCharacter_Pipe		4
#define SpecialCharacter_Asterisk	5

#endif /* DISPLAYCHARACTERS_HPP_*/
