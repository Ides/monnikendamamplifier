/*
 * Definitions.h
 *
 *  Created on: 5 nov. 2020
 *      Author: Matthijs
 */

#ifndef DEFINITIONS_H_
#define DEFINITIONS_H_


enum display_mode
{
	// Displays the encoder value
	displaymode_encoder = 0,
	// Displays the set text 'forever'
	displaymode_text = 1,
	// Display the set text once (scrolling if needed) and then returns to encoder value display
	displaymode_text_once = 2,
};


#endif /* DEFINITIONS_H_ */
