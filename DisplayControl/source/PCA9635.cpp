/*
 * I2C.cpp
 *
 *  Created on: 14 Dec 2018
 *      Author: Matthijs
 */

#include <PCA9635.h>

PCA9635::PCA9635(I2C *i2c, uint8_t slaveAddress)
{
	_i2c = i2c;
	SlaveAddress = slaveAddress;
}

PCA9635::~PCA9635()
{
//	delete _i2c;
//	_i2c = NULL;
}

void PCA9635::PullRegisters()
{
	uint8_t i;
	Read(0x00, RemoteRegisters, REG_COUNT);
	for(i = 0; i < REG_COUNT; i++)
		LocalRegisters[i] = RemoteRegisters[i];

	RegistersSync = true;
}

void PCA9635::PushRegisters()
{
	if(RegistersSync)
		return;

	uint8_t i;
	for(i = 0; i < REG_COUNT; i++)
	{
		if(LocalRegisters[i] != RemoteRegisters[i])
		{
			Write(i, LocalRegisters + i, 1);
			RemoteRegisters[i] = LocalRegisters[i];
		}
	}

	RegistersSync = true;
}

void PCA9635::SetRegister(uint8_t registerIndex, uint8_t registerValue)
{
	// Check if we are in the correct register range
	if(registerIndex >= REG_COUNT)
		return;

	// Set the local register to the correct value
	LocalRegisters[registerIndex] = registerValue;

	// If the register is changed...
	if(LocalRegisters[registerIndex] != RemoteRegisters[registerIndex])
		// ..set sync to false
		RegistersSync = false;
}

void PCA9635::SetAllPWMRegisters(uint8_t registerValue)
{
	uint8_t i = 0;

	// Set all the local PWMx registers to the correct value
	for(i = REG_PWM0; i <= REG_PWM15; i++)
		LocalRegisters[i] = registerValue;

	RegistersSync = false;
}

void PCA9635::SetOutput(uint8_t outputIndex, output_state state)
{
	// If the output index is larger than 15...
	if(outputIndex > 15)
		// ..we are out of range so we return
		return;

	// To get the offset of the register we need to change for this output index, we divide by 4
	// (0-3 -> 0; 4-7 -> 1; 8-11 -> 2; 12-15 -> 3)
	uint8_t registerOffset = outputIndex / 4;

	// To get the index within the byte we need to change, we do a modulo 4
	uint8_t bitsIndex = outputIndex % 4;

	// Get the mask of the bits we need to overwrite
	uint8_t mask = 0x03 << (2 * bitsIndex);

    // Get the original value without the masked bits and add the state shifted to the correct bit position and mask them to ake sure there are no extra bits
	SetRegister(REG_LEDOUT0 + registerOffset, (LocalRegisters[REG_LEDOUT0 + registerOffset] & ~mask) + ((state << (2 * bitsIndex)) & mask));
	//LocalRegisters[REG_LEDOUT0 + registerOffset] = (LocalRegisters[REG_LEDOUT0 + registerOffset] & ~mask) + ((state << (2 * bitsIndex)) & mask);
}

void PCA9635::SetAllOutputs(output_state toState)
{
	// Make a byte value for 4 output states
	uint8_t stateByteValue;
	switch(toState)
	{
	case OutputState_Off:
		stateByteValue = 0x00;
		break;
	case OutputState_FullOn:
		stateByteValue = 0x55;
		break;
	case OutputState_PWMControlledOn:
		stateByteValue = 0xAA;
		break;
	case OutputState_PWMAndGlobalControlledOn:
		stateByteValue = 0xFF;
		break;
	default:
		stateByteValue = 0x00;
		break;
	}

	// And set all the led output registeres to that value!
	LocalRegisters[REG_LEDOUT0] = stateByteValue;
	LocalRegisters[REG_LEDOUT1] = stateByteValue;
	LocalRegisters[REG_LEDOUT2] = stateByteValue;
	LocalRegisters[REG_LEDOUT3] = stateByteValue;
}


/*****************************
*  PRIVATE FUNCTIONS         *
*****************************/

int PCA9635::Write(uint8_t SubAddress, uint8_t *buffer, size_t length)
{
	i2c_master_transfer_t masterXfer = {0};
	status_t retVal = kStatus_Fail;

    masterXfer.slaveAddress = SlaveAddress;
    masterXfer.direction = kI2C_Write;
    // We OR the sub-address with 0x80 to turn auto increment on for the buffer index
    masterXfer.subaddress = (uint32_t)(SubAddress | 0x80);
    masterXfer.subaddressSize = 1;
    masterXfer.data = buffer;
    masterXfer.dataSize = length;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    retVal = _i2c->Transfer(&masterXfer);
    /* Send master non-blocking data to slave */
    //retVal = I2C_MasterTransferNonBlocking(I2C1, &g_Matrix_I2C_Handle, &masterXfer);


    if (retVal != kStatus_Success)
    {
        return -1;
    }

    /*  Wait for transfer completed. */
    while (_i2c->I2C_TransferBusy)
    {
    }

    if(_i2c->I2C_TransferSuccess)
    	return 0;
    else
    	return -1;
}

int PCA9635::Read(uint8_t SubAddress, uint8_t *buffer, size_t length)
{
	i2c_master_transfer_t masterXfer = {0};
	status_t retVal = kStatus_Fail;

    masterXfer.slaveAddress = SlaveAddress;
    masterXfer.direction = kI2C_Read;
    // We OR the sub-address with 0x80 to turn auto increment on for the buffer index
    masterXfer.subaddress = (uint32_t)(SubAddress | 0x80);
    masterXfer.subaddressSize = 1;
    masterXfer.data = buffer;
    masterXfer.dataSize = length;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    /* Send master non-blocking data to slave */
    //retVal = I2C_MasterTransferNonBlocking(I2C1, &g_Matrix_I2C_Handle, &masterXfer);
    retVal = _i2c->Transfer(&masterXfer);

    if (retVal != kStatus_Success)
    {
        return -1;
    }

    /*  Wait for transfer completed. */
    while (_i2c->I2C_TransferBusy)
    {
    }

    if(_i2c->I2C_TransferSuccess)
    	return 0;
    else
    	return -1;
}
