/*
 * LEDManager.h
 *
 *  Created on: 14 Dec 2018
 *      Author: Matthijs
 */

#ifndef LEDMANAGER_H_
#define LEDMANAGER_H_

#include "fsl_i2c.h"
#include "I2C.h"
#include "PCA9635.h"


struct LedAddress
{
	uint8_t DriverIndex;
	uint8_t DriverOutputChannel;
};

struct ZoneAddresses
{
	LedAddress ButtonLed;
	LedAddress Input1_GreenLed;
	LedAddress Input2_GreenLed;
	LedAddress Input1_RedLed;
	LedAddress Input2_RedLed;
};

enum zone_led_mask
{
	ZoneState_Input1Green = 1,
	ZoneState_Input1Red = 2,
	ZoneState_Input2Green = 4,
	ZoneState_Input2Red = 8,
	ZoneState_ButtonLed = 16,
//	ZoneState_Editing = 32,
//	ZoneState_Fault = 64
};



class LEDManager
{
private:
	uint8_t SlaveAddress;
	I2C *_i2c;

	// The Led settings for the 8 zones we have
	ZoneAddresses zoneAddresses[8];
	// The preset led addresses
	LedAddress PresetLed[3];

	LedAddress ControlInput1_GreenLed;
	LedAddress ControlInput2_GreenLed;
	LedAddress ControlInput1_RedLed;
	LedAddress ControlInput2_RedLed;

	// The pointers to the 3 led driver classes
	PCA9635* LedDriver[3];

	void PushAllDrivers(void);
	void SetLedAddresses(void);
	void SetDriverOutput(uint8_t driverIndex, uint8_t ledIndex, bool setOn);
public:
	// Constructor
	LEDManager(I2C_Type *i2c_base);
	// Destructor
	virtual ~LEDManager();

	void SetZoneState(uint8_t zoneIndex, zone_led_mask state);
	void SetZoneState(uint8_t zoneIndex, zone_led_mask state, zone_led_mask stateMask);
	void SetMasterVolumeLeds(uint8_t channel0, uint8_t channel1);
	void SetPresetLed(uint8_t SelectedPreset);
	//void SetButtonLed(uint8_t ButtonIndex, bool state);
};

#endif /* LEDMANAGER_H_*/
