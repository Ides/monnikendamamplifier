#ifndef LEDS_H_
#define LEDS_H_

#include <I2C.h>
#include "LPC54113.h"

#include "LPC54113.h"


//#include <stdio.h>
//#include "board.h"
//#include "peripherals.h"
//#include "pin_mux.h"
//#include "clock_config.h"
//#include "LPC54113.h"
//#include "fsl_debug_console.h"

void Init_LEDs(I2C * i2c);
void LEDs_AllRedOn();
void LEDs_AllRedOff();
void LEDs_AllGreenOn();
void LEDs_AllGreenOff();
void LEDs_AllButtonsOn();
void LEDs_AllButtonsOff();
void LEDs_SetChannelLEDs(uint8_t channel, uint8_t value);
void LEDs_SetGlobalPWM(uint8_t pwm);
void LEDs_DumpRegisters();

#endif // LEDS_H_
