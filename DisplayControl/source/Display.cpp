/*
 * Display.cpp
 *
 *  Created on: 31 Mar 2019
 *      Author: Matthijs
 */
#include "Display.h"
#include "DisplayCharacters.hpp"
#include "fsl_debug_console.h"

I2C *Display_I2C;

void Init_Display(I2C * i2c)
{
	Display_I2C = i2c;

	uint8_t buf[10] = {0,0,0,0,0,0,0,0,0,0};

    uint8_t DisplayIndex = 1;
    for(DisplayIndex = 0; DisplayIndex < 4; DisplayIndex++)
    {
		Display_Read(0x00, buf, 1, DisplayIndex);

		//printf("Mode1 = 0x%02X\r\n", buf[0]);
		// Set the controller in normal mode
		buf[0] &= ~0x10;
		Display_Write(0x00, buf, 1, DisplayIndex);

		Display_Read(0x01, buf, 1, DisplayIndex);
		buf[0] &= ~0x20;
		Display_Write(0x01, buf, 1, DisplayIndex);
		//printf("Mode2 = 0x%02X\r\n", buf[0]);

		// Set the freq register so the period will be 1 sec
//		buf[0] = 0x17;
//		Display_Write(0x13, buf, 1, DisplayIndex);

		// Enable the output 0-15 to PWM mode
		//buf[0] = 0xAA;
		//Display_Write(0x14, buf, 1, DisplayIndex);
		//Display_Write(0x15, buf, 1, DisplayIndex);
		//Display_Write(0x16, buf, 1, DisplayIndex);
		//Display_Write(0x17, buf, 1, DisplayIndex);
		//WriteCharacterToDisplay('A', DisplayIndex);

		// Set the PWM for 0-3 to 50%
		buf[0] = 0x80;
		Display_Write(0x02, buf, 1, DisplayIndex);
		Display_Write(0x03, buf, 1, DisplayIndex);
		Display_Write(0x04, buf, 1, DisplayIndex);
		Display_Write(0x05, buf, 1, DisplayIndex);
		Display_Write(0x06, buf, 1, DisplayIndex);
		Display_Write(0x07, buf, 1, DisplayIndex);
		Display_Write(0x08, buf, 1, DisplayIndex);
		Display_Write(0x09, buf, 1, DisplayIndex);
		Display_Write(0x0A, buf, 1, DisplayIndex);
		Display_Write(0x0B, buf, 1, DisplayIndex);
		Display_Write(0x0C, buf, 1, DisplayIndex);
		Display_Write(0x0D, buf, 1, DisplayIndex);
		Display_Write(0x0E, buf, 1, DisplayIndex);
		Display_Write(0x0F, buf, 1, DisplayIndex);
		Display_Write(0x10, buf, 1, DisplayIndex);
		Display_Write(0x11, buf, 1, DisplayIndex);
    }
}

int Display_Write(uint8_t SubAddress, uint8_t *buffer, size_t length, uint8_t displayIndex)
{
	i2c_master_transfer_t masterXfer = {0};
	status_t retVal = kStatus_Fail;

	if(displayIndex == 3)
		displayIndex = 7;


    masterXfer.slaveAddress = displayIndex;
    masterXfer.direction = kI2C_Write;
    masterXfer.subaddress = (uint32_t)SubAddress;
    masterXfer.subaddressSize = 1;
    masterXfer.data = buffer;
    masterXfer.dataSize = length;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    retVal = Display_I2C->Transfer(&masterXfer);
    /* Send master non-blocking data to slave */
    //retVal = I2C_MasterTransferNonBlocking(I2C1, &g_Matrix_I2C_Handle, &masterXfer);


    if (retVal != kStatus_Success)
    {
        return -1;
    }

    /*  Wait for transfer completed. */
    while (Display_I2C->I2C_TransferBusy)
    {
    }

    if(Display_I2C->I2C_TransferSuccess)
    	return 0;
    else
    	return -1;
}

int Display_Read(uint8_t SubAddress, uint8_t *buffer, size_t length, uint8_t displayIndex)
{
	i2c_master_transfer_t masterXfer = {0};
	status_t retVal = kStatus_Fail;

	// We had to change the address for the 3rd (left most?) display, because 3 was reserved :/
	if(displayIndex == 3)
		displayIndex = 7;

    masterXfer.slaveAddress = displayIndex;
    masterXfer.direction = kI2C_Read;
    masterXfer.subaddress = (uint32_t)SubAddress;
    masterXfer.subaddressSize = 1;
    masterXfer.data = buffer;
    masterXfer.dataSize = length;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    /* Send master non-blocking data to slave */
    //retVal = I2C_MasterTransferNonBlocking(I2C1, &g_Matrix_I2C_Handle, &masterXfer);
    retVal = Display_I2C->Transfer(&masterXfer);

    if (retVal != kStatus_Success)
    {
        return -1;
    }

    /*  Wait for transfer completed. */
    while (Display_I2C->I2C_TransferBusy)
    {
    }

    if(Display_I2C->I2C_TransferSuccess)
    	return 0;
    else
    	return -1;
}

void WriteToDisplay(std::string text)
{
	WriteToDisplay(text, 0);
}

void WriteToDisplay(std::string text, uint8_t offset)
{
	uint8_t i = 0;
	for(i = 0; i < 4; i++)
	{
		// If there is a character in the string at this position, display it...
		if((i + offset) < text.length())
			WriteCharacterToDisplay(text[i + offset], 3 - i);
		// else, just display a space to blank out the display
		else
			WriteCharacterToDisplay(' ', 3 - i);
	}
}

void WriteToDisplay(uint8_t *text, int8_t offset)
{
	uint8_t i = 0;
	uint8_t textLength = 0;
	for(i = 0; i < 32; i++)
	{
		if(text[i] == 0)
		{
			textLength = i;
			break;
		}
	}

	for(i = 0; i < 4; i++)
	{
		// If we are in the negative offset...
		if(i + offset < 0)
			// ..display a space to blank out the display
			WriteCharacterToDisplay(' ', 3 - i);
		// Else if there is a character in the string at this position, display it...
		else if((i + offset) < textLength)
			WriteCharacterToDisplay(text[i + offset], 3 - i);
		// else, just display a space to blank out the display
		else
			WriteCharacterToDisplay(' ', 3 - i);
	}
}


void WriteNumberValueToDisplay(int8_t valueToDisplay)
{
	bool isNegative = (valueToDisplay < 0);

	// We need a uint8_t to hold the absolute value to display, because we could potentially have -128 and 128 won't fit in an int8_t...
	uint8_t absValueToDisplay;

	// If negative...
	if(isNegative)
		// ..get the absolute value
		absValueToDisplay = -valueToDisplay;
	else
		// .. else we get the normal value
		absValueToDisplay = valueToDisplay;

	if(absValueToDisplay < 10)
	{
		WriteCharacterToDisplay(0, 3);
		WriteCharacterToDisplay(0, 2);
		WriteCharacterToDisplay(isNegative ? '-' : 0, 1);
		WriteCharacterToDisplay('0' + absValueToDisplay % 10, 0);
	}
	else if(absValueToDisplay < 100)
	{
		WriteCharacterToDisplay(0, 3);
		WriteCharacterToDisplay(isNegative ? '-' : 0, 2);
		WriteCharacterToDisplay('0' + (absValueToDisplay / 10) % 10, 1);
		WriteCharacterToDisplay('0' + absValueToDisplay % 10, 0);
	}
	else
	{
		WriteCharacterToDisplay(isNegative ? '-' : 0, 3);
		WriteCharacterToDisplay('0' + (absValueToDisplay / 100) % 10, 2);
		WriteCharacterToDisplay('0' + (absValueToDisplay / 10) % 10, 1);
		WriteCharacterToDisplay('0' + absValueToDisplay % 10, 0);
	}
}

void WriteCharacterToDisplay(uint8_t ASCIICharacter, uint8_t displayIndex)
{
	uint16_t BitValue = 0x0000;

	if(ASCIICharacter == 0)
	{
		BitValue = 0x0000;
	}
	// If it is a upper case letter...
	else if(ASCIICharacter >= 'A' && ASCIICharacter <= 'Z')
	{
		ASCIICharacter -= 'A';
		BitValue = DisplayLetters[ASCIICharacter];
	}
	// If it is a lower case letter, we treat it like a upper case letter
	else if(ASCIICharacter >= 'a' && ASCIICharacter <= 'z')
	{
		ASCIICharacter -= 'a';
		BitValue = DisplayLetters[ASCIICharacter];
	}
	// if it is a number
	else if(ASCIICharacter >= '0' && ASCIICharacter <= '9')
	{
		ASCIICharacter -= '0';
		BitValue = DisplayNumbers[ASCIICharacter];
	}
	else if(ASCIICharacter == '+')
		BitValue = DisplaySpecialCharacters[SpecialCharacter_Plus];
	else if(ASCIICharacter == '-')
		BitValue = DisplaySpecialCharacters[SpecialCharacter_Minus];
	else if(ASCIICharacter == '/')
		BitValue = DisplaySpecialCharacters[SpecialCharacter_Slash];
	else if(ASCIICharacter == '\\')
		BitValue = DisplaySpecialCharacters[SpecialCharacter_BSlash];
	else if(ASCIICharacter == '|')
		BitValue = DisplaySpecialCharacters[SpecialCharacter_Pipe];
	else if(ASCIICharacter == '*')
		BitValue = DisplaySpecialCharacters[SpecialCharacter_Asterisk];


	//
	uint8_t NewLedOnValue = 0xC0;

	uint8_t charBuf[4] = {0, 0, 0, 0};

	uint8_t i;
	for(i = 0; i < 16; i++)
	{
		charBuf[i / 4] = charBuf[i / 4] >> 2;
		charBuf[i / 4] |= (BitValue & 0x0001 << i) == 0 ? 0x00 : NewLedOnValue;
	}

	Display_Write(0x14 | 0x80, charBuf, 4, displayIndex);
}

void Display_DumpRegisters()
{
	uint8_t reg[1];
	uint8_t i, ChipIndex;

	for(i = 0; i < 0x18; i++)
	{
		printf("Reg 0x%02X:", i);
		for(ChipIndex = 0; ChipIndex < 4; ChipIndex++)
		{
			Display_Read(i, reg, 1, ChipIndex);
			printf(" %02X", reg[0]);
		}
		printf("\r\n");
	}
	printf("\r\n");
}
