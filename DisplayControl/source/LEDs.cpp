/*
 * LEDs.cpp
 *
 *  Created on: 31 Mar 2019
 *      Author: Matthijs
 */
#include "LEDs.h"
#include "fsl_debug_console.h"

I2C *LEDs_I2C;

int Chip_Read(uint8_t SubAddress, uint8_t *buffer, size_t length, uint8_t chipIndex);
int Chip_Write(uint8_t SubAddress, uint8_t *buffer, size_t length, uint8_t chipIndex);

void Init_LEDs(I2C * i2c)
{
	LEDs_I2C = i2c;

	uint8_t buf[10] = {0,0,0,0,0,0,0,0,0,0};

    uint8_t DisplayIndex = 0;
    for(DisplayIndex = 0; DisplayIndex < 3; DisplayIndex++)
    {
    	Chip_Read(0x00, buf, 1, DisplayIndex);

//		printf("Mode1 = 0x%02X\r\n", buf[0]);
		// Set the controller in normal mode
		buf[0] &= ~0x10;
		Chip_Write(0x00, buf, 1, DisplayIndex);
//		printf("Mode1 = 0x%02X\r\n", buf[0]);

		Chip_Read(0x01, buf, 1, DisplayIndex);
//		printf("Mode2 = 0x%02X\r\n", buf[0]);
		buf[0] &= ~0x20;
		buf[0] |= 0x10;
		Chip_Write(0x01, buf, 1, DisplayIndex);
//		printf("Mode2 = 0x%02X\r\n", buf[0]);

		// Set the PWM for 0-3 to 100%
		buf[0] = 0xFF;
		Chip_Write(0x02, buf, 1, DisplayIndex);
		Chip_Write(0x03, buf, 1, DisplayIndex);
		Chip_Write(0x04, buf, 1, DisplayIndex);
		Chip_Write(0x05, buf, 1, DisplayIndex);
		Chip_Write(0x06, buf, 1, DisplayIndex);
		Chip_Write(0x07, buf, 1, DisplayIndex);
		Chip_Write(0x08, buf, 1, DisplayIndex);
		Chip_Write(0x09, buf, 1, DisplayIndex);
		Chip_Write(0x0A, buf, 1, DisplayIndex);
		Chip_Write(0x0B, buf, 1, DisplayIndex);
		Chip_Write(0x0C, buf, 1, DisplayIndex);
		Chip_Write(0x0D, buf, 1, DisplayIndex);
		Chip_Write(0x0E, buf, 1, DisplayIndex);
		Chip_Write(0x0F, buf, 1, DisplayIndex);
		Chip_Write(0x10, buf, 1, DisplayIndex);
		Chip_Write(0x11, buf, 1, DisplayIndex);

		// Group PWM to 100%
		buf[0] = 0x80;
		Chip_Write(0x12, buf, 1, DisplayIndex);

		buf[0] = 0x00;
		Chip_Write(0x14, buf, 1, DisplayIndex);
		Chip_Write(0x15, buf, 1, DisplayIndex);
		Chip_Write(0x16, buf, 1, DisplayIndex);
		Chip_Write(0x17, buf, 1, DisplayIndex);
    }
}

int Chip_Write(uint8_t SubAddress, uint8_t *buffer, size_t length, uint8_t chipIndex)
{
	i2c_master_transfer_t masterXfer = {0};
	status_t retVal = kStatus_Fail;

	// Set the auto increment flag by default.
	SubAddress = SubAddress | 0x80;

    masterXfer.slaveAddress = chipIndex;
    masterXfer.direction = kI2C_Write;
    masterXfer.subaddress = (uint32_t)SubAddress;
    masterXfer.subaddressSize = 1;
    masterXfer.data = buffer;
    masterXfer.dataSize = length;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    retVal = LEDs_I2C->Transfer(&masterXfer);
    /* Send master non-blocking data to slave */
    //retVal = I2C_MasterTransferNonBlocking(I2C1, &g_Matrix_I2C_Handle, &masterXfer);


    if (retVal != kStatus_Success)
    {
        return -1;
    }

    /*  Wait for transfer completed. */
    while (LEDs_I2C->I2C_TransferBusy)
    {
    }

    if(LEDs_I2C->I2C_TransferSuccess)
    	return 0;
    else
    	return -1;
}

int Chip_Read(uint8_t SubAddress, uint8_t *buffer, size_t length, uint8_t chipIndex)
{
	i2c_master_transfer_t masterXfer = {0};
	status_t retVal = kStatus_Fail;

	// Set the auto increment flag by default.
	SubAddress = SubAddress | 0x80;

    masterXfer.slaveAddress = chipIndex;
    masterXfer.direction = kI2C_Read;
    masterXfer.subaddress = (uint32_t)SubAddress;
    masterXfer.subaddressSize = 1;
    masterXfer.data = buffer;
    masterXfer.dataSize = length;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    /* Send master non-blocking data to slave */
    //retVal = I2C_MasterTransferNonBlocking(I2C1, &g_Matrix_I2C_Handle, &masterXfer);
    retVal = LEDs_I2C->Transfer(&masterXfer);

    if (retVal != kStatus_Success)
    {
        return -1;
    }

    /*  Wait for transfer completed. */
    while (LEDs_I2C->I2C_TransferBusy)
    {
    }

    if(LEDs_I2C->I2C_TransferSuccess)
    	return 0;
    else
    	return -1;
}

void LEDs_SetChannelLEDs(uint8_t channel, uint8_t value)
{

	// If the new value has the Green Led for Input 1 set to ON
	if(value & 0x01)
	{

	}
}

void LEDs_AllRedOn()
{
	uint8_t buf[4] = {0xFF, 0xFF, 0xFF, 0xFF };

	Chip_Write(0x14, buf, 4, 1);

	// Read current value of chip 2 for buffers containing the RED leds
	Chip_Read(0x16,  buf, 2, 2);
	// Set the red leds to on
	buf[0] |= 0x0C;
	buf[1] |= 0xC0;
	// Write the bytes back
	Chip_Write(0x16, buf, 2, 2);
}

void LEDs_AllRedOff()
{
	uint8_t buf[4] = {0x00, 0x00, 0x00, 0x00 };

	Chip_Write(0x14, buf, 4, 1);

	// Read current value of chip 2 for buffers containing the RED leds
	Chip_Read(0x16,  buf, 2, 2);
	// clear the red leds to off
	buf[0] &= ~0xC0;
	buf[1] &= ~0x0C;
	// Write the bytes back
	Chip_Write(0x16, buf, 2, 2);
}

void LEDs_AllGreenOn()
{
	uint8_t buf[4] = {0xFF, 0xFF, 0xFF, 0xFF };

	Chip_Write(0x14, buf, 4, 0);

	// Read current value of chip 2 for buffers containing the GREEN leds
	Chip_Read(0x17,  buf, 1, 2);
	// Set the green leds to on
	buf[0] |= 0x33;

	// Write the bytes back
	Chip_Write(0x17, buf, 1, 2);
}

void LEDs_AllGreenOff()
{
	uint8_t buf[4] = {0x00, 0x00, 0x00, 0x00 };

	Chip_Write(0x14, buf, 4, 0);

	// Read current value of chip 2 for buffers containing the GREEN leds
	Chip_Read(0x17,  buf, 1, 2);
	// clear the green leds to off
	buf[0] &= ~0x33;
	// Write the bytes back
	Chip_Write(0x17, buf, 1, 2);
}

void LEDs_AllButtonsOn()
{
	uint8_t buf[3];

	Chip_Read(0x14, buf, 3, 2);
	buf[0] = 0xFF;
	buf[1] = 0xFF;
	buf[2] |= 0x3F;
	Chip_Write(0x14, buf, 3, 2);
}
void LEDs_AllButtonsOff()
{
	uint8_t buf[3];

	Chip_Read(0x14, buf, 3, 2);
	buf[0] = 0x00;
	buf[1] = 0x00;
	buf[2] &= ~0x3F;
	Chip_Write(0x14, buf, 3, 2);
}

void LEDs_SetGlobalPWM(uint8_t pwm)
{
	uint8_t buf[1] = {pwm};
	Chip_Write(0x12, buf, 1, 0);
	Chip_Write(0x12, buf, 1, 1);
	Chip_Write(0x12, buf, 1, 2);
}

void LEDs_DumpRegisters()
{
	uint8_t reg[1];
	uint8_t i, ChipIndex;

	for(i = 0; i < 0x18; i++)
	{
		printf("Reg 0x%02X:", i);
		for(ChipIndex = 0; ChipIndex < 3; ChipIndex++)
		{
			Chip_Read(i, reg, 1, ChipIndex);
			printf(" %02X", reg[0]);
		}
		printf("\r\n");
	}
	printf("\r\n");
}
