#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <I2C.h>
#include "LPC54113.h"

#include "LPC54113.h"
#include <string>

//#include <stdio.h>
//#include "board.h"
//#include "peripherals.h"
//#include "pin_mux.h"
//#include "clock_config.h"
//#include "LPC54113.h"
//#include "fsl_debug_console.h"

void Init_Display(I2C * i2c);
int Display_Write(uint8_t SubAddress, uint8_t *buffer, size_t length, uint8_t displayIndex);
int Display_Read(uint8_t SubAddress, uint8_t *buffer, size_t length, uint8_t displayIndex);
void WriteNumberValueToDisplay(int8_t valueToDisplay);
void WriteToDisplay(std::string text);
void WriteToDisplay(std::string text, uint8_t offset);
void WriteToDisplay(uint8_t *text, int8_t offset);
void WriteCharacterToDisplay(uint8_t character, uint8_t displayIndex);
void Display_DumpRegisters();
#endif // DISPLAY_H_
