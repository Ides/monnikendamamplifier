/*
 * PCA9635.h
 *
 *  Created on: 14 Dec 2018
 *      Author: Matthijs
 */

#ifndef PCA9635_H_
#define PCA9635_H_

#include "fsl_i2c.h"
#include "I2C.h"

#define REG_MODE1		0x00
#define REG_MODE2		0x01
#define REG_PWM0		0x02
#define REG_PWM1		0x03
#define REG_PWM2		0x04
#define REG_PWM3		0x05
#define REG_PWM4		0x06
#define REG_PWM5		0x07
#define REG_PWM6		0x08
#define REG_PWM7		0x09
#define REG_PWM8		0x0A
#define REG_PWM9		0x0B
#define REG_PWM10		0x0C
#define REG_PWM11		0x0D
#define REG_PWM12		0x0E
#define REG_PWM13		0x0F
#define REG_PWM14		0x10
#define REG_PWM15		0x11
#define REG_GRPPWM		0x12
#define REG_GRPFREQ		0x13
#define REG_LEDOUT0		0x14
#define REG_LEDOUT1		0x15
#define REG_LEDOUT2		0x16
#define REG_LEDOUT3		0x17
#define REG_SUBADR1		0x18
#define REG_SUBADR2		0x19
#define REG_SUBADR3		0x1A
#define REG_ALLCALLADR	0x1B

#define REG_COUNT		0x1C

enum output_state
{
	OutputState_Off = 0,
	OutputState_FullOn = 1,
	OutputState_PWMControlledOn = 2,
	OutputState_PWMAndGlobalControlledOn = 3
};

class PCA9635
{
private:

	uint8_t RemoteRegisters[REG_COUNT];
	uint8_t LocalRegisters[REG_COUNT];
	bool RegistersSync = false;
	uint8_t SlaveAddress;
	I2C *_i2c;

	int Write(uint8_t SubAddress, uint8_t *buffer, size_t length);
	int Read(uint8_t SubAddress, uint8_t *buffer, size_t length);

public:
	// Constructor
	PCA9635(I2C *i2c, uint8_t slaveAddress);
	// Destructor
	virtual ~PCA9635();

	// Overwrites all the local register values with the register values of the PCA9635
	void PullRegisters();
	// Overwrites all the PCA9635 register values with the local register values
	void PushRegisters();

	// Sets the LOCAL register to the provided value
	void SetRegister(uint8_t registerIndex, uint8_t registerValue);

	// Sets all the PWM Control registers to the provided value
	void SetAllPWMRegisters(uint8_t registerValue);

	// Set a specific output to the provided state
	void SetOutput(uint8_t outputIndex, output_state state);
	// Set all outputs to the provided state

	void SetAllOutputs(output_state toState);
};

#endif /* PCA9635_H_*/
