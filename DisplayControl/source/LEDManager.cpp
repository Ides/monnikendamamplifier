/*
 * LEDManager.cpp
 *
 *  Created on: 14 Dec 2018
 *      Author: Matthijs
 */

#include <LEDManager.h>

#define ADDRES_LED_DRIVER0	0x00
#define ADDRES_LED_DRIVER1	0x01
#define ADDRES_LED_DRIVER2	0x02


LEDManager::LEDManager(I2C_Type *i2c_base)
{
	// Create an instance of the I2C class
	_i2c = new I2C(i2c_base);

	// Create the Led Driver classes
	LedDriver[0] = new PCA9635(_i2c, ADDRES_LED_DRIVER0);
	LedDriver[1] = new PCA9635(_i2c, ADDRES_LED_DRIVER1);
	LedDriver[2] = new PCA9635(_i2c, ADDRES_LED_DRIVER2);

    uint8_t DisplayIndex = 0;
    for(DisplayIndex = 0; DisplayIndex < 3; DisplayIndex++)
    {
    	// Read and store all the register values from the drivers
    	LedDriver[DisplayIndex]->PullRegisters();

		// Set the mode1 register to the default values with the only change that b4 is cleared to disable the sleep mode
		LedDriver[DisplayIndex]->SetRegister(REG_MODE1, 0x81);

		// Set the mode2 register to the default values with the only change that b4 is set to invert output logic
		LedDriver[DisplayIndex]->SetRegister(REG_MODE2, 0x15);

		// Set all the individual LED PWM Registers to full brightness
		LedDriver[DisplayIndex]->SetAllPWMRegisters(0xFF);

		// Set the overall group dimming to half brightness
		LedDriver[DisplayIndex]->SetRegister(REG_GRPPWM, 0x80);

		// Set all the leds to off
		LedDriver[DisplayIndex]->SetAllOutputs(OutputState_Off);


		// Send all the altered registers to the device!
		LedDriver[DisplayIndex]->PushRegisters();
    }


	// Set all the address we need to use for addressing any of the leds
	SetLedAddresses();

  }

LEDManager::~LEDManager()
{
//	delete _i2c;
	_i2c  = NULL;
}



/*
void LEDManager::SetButtonLed(uint8_t ButtonIndex, bool state)
{
	uint8_t DriverIndex;
	uint8_t OutputChannel;

	// If button index is below 8...
	if(ButtonIndex < 8)
	{
		// It is one of the zone buttons
		DriverIndex = zoneAddresses[ButtonIndex].ButtonLed.DriverIndex;
		OutputChannel = zoneAddresses[ButtonIndex].ButtonLed.DriverOutputChannel;
	}
	// else if the index is 8-10...
	else if (ButtonIndex < 11)
	{
		// It is one of the preset buttons
		DriverIndex = PresetLed[ButtonIndex - 8].DriverIndex;
		OutputChannel = PresetLed[ButtonIndex - 8].DriverOutputChannel;
	}
	// If 11 or higher, we are out of range....
	else
		return;

	// Set the found button led to the provided state (for now, on is set to PWM and Global Control....
	LedDriver[DriverIndex]->SetOutput(OutputChannel, state ? OutputState_PWMAndGlobalControlledOn : OutputState_Off);
}*/

void LEDManager::SetPresetLed(uint8_t SelectedPreset)
{
	for(uint8_t i = 0; i < 3; i++)
		SetDriverOutput(PresetLed[i].DriverIndex, PresetLed[i].DriverOutputChannel, (SelectedPreset == (i+1)));

	PushAllDrivers();
}

void LEDManager::SetMasterVolumeLeds(uint8_t channel0, uint8_t channel1)
{
	SetDriverOutput(ControlInput1_GreenLed.DriverIndex, ControlInput1_GreenLed.DriverOutputChannel, (channel0 & 0x01) != 0);
	SetDriverOutput(ControlInput1_RedLed.DriverIndex, ControlInput1_RedLed.DriverOutputChannel, (channel0 & 0x02) != 0);
	SetDriverOutput(ControlInput2_GreenLed.DriverIndex, ControlInput2_GreenLed.DriverOutputChannel, (channel1 & 0x01) != 0);
	SetDriverOutput(ControlInput2_RedLed.DriverIndex, ControlInput2_RedLed.DriverOutputChannel, (channel1 & 0x02) != 0);

	PushAllDrivers();
}


void LEDManager::SetZoneState(uint8_t zoneIndex, zone_led_mask state)
{
	// If no mask is provided, we assume all state values are valid!
	// Call the function with the full mask on
	SetZoneState(zoneIndex, state, (zone_led_mask)0xFF);
}

void LEDManager::SetZoneState(uint8_t zoneIndex, zone_led_mask state, zone_led_mask stateMask)
{
	if((stateMask & ZoneState_Input1Green) > 0)
		SetDriverOutput(zoneAddresses[zoneIndex].Input1_GreenLed.DriverIndex, zoneAddresses[zoneIndex].Input1_GreenLed.DriverOutputChannel, (state & ZoneState_Input1Green) != 0);
	if((stateMask & ZoneState_Input1Red) > 0)
		SetDriverOutput(zoneAddresses[zoneIndex].Input1_RedLed.DriverIndex, zoneAddresses[zoneIndex].Input1_RedLed.DriverOutputChannel, (state & ZoneState_Input1Red) != 0);
	if((stateMask & ZoneState_Input2Green) > 0)
		SetDriverOutput(zoneAddresses[zoneIndex].Input2_GreenLed.DriverIndex, zoneAddresses[zoneIndex].Input2_GreenLed.DriverOutputChannel, (state & ZoneState_Input2Green) != 0);
	if((stateMask & ZoneState_Input2Red) > 0)
		SetDriverOutput(zoneAddresses[zoneIndex].Input2_RedLed.DriverIndex, zoneAddresses[zoneIndex].Input2_RedLed.DriverOutputChannel, (state & ZoneState_Input2Red) != 0);

	if((stateMask & ZoneState_ButtonLed) > 0)
		SetDriverOutput(zoneAddresses[zoneIndex].ButtonLed.DriverIndex, zoneAddresses[zoneIndex].ButtonLed.DriverOutputChannel, (state & ZoneState_ButtonLed) != 0);

	PushAllDrivers();
}



//// PRIVATE FUNCTIONS

void LEDManager::PushAllDrivers(void)
{
	LedDriver[0]->PushRegisters();
	LedDriver[1]->PushRegisters();
	LedDriver[2]->PushRegisters();
}

void LEDManager::SetDriverOutput(uint8_t driverIndex, uint8_t ledIndex, bool setOn)
{
	// TODO: When set to ON, we need to be able to have something else than OutputState_PWMAndGlobalControlledOn....
	LedDriver[driverIndex]->SetOutput(ledIndex,	setOn ? OutputState_PWMAndGlobalControlledOn : OutputState_Off);
}


void LEDManager::SetLedAddresses(void)
{
	uint8_t i;

	for(i = 0; i < 8; i++)
	{
		// All zone buttons are on driver 2
		zoneAddresses[i].ButtonLed.DriverIndex = 2;
		// The output channel is the index of the channel (0-7)
		zoneAddresses[i].ButtonLed.DriverOutputChannel = i;
		// The green zone leds are all on driver 0

		zoneAddresses[i].Input1_GreenLed.DriverIndex = 0;
		// The green zone 1 leds are at output 0-7
		zoneAddresses[i].Input1_GreenLed.DriverOutputChannel = i;

		// The green zone leds are all on driver 0
		zoneAddresses[i].Input2_GreenLed.DriverIndex = 0;
		// The green zone 2 leds are at output 8-15
		zoneAddresses[i].Input2_GreenLed.DriverOutputChannel = i+8;

		// The red zone leds are all on driver 1
		zoneAddresses[i].Input1_RedLed.DriverIndex = 1;
		// The red zone 1 leds are at output 0-7
		zoneAddresses[i].Input1_RedLed.DriverOutputChannel = i;

		// The red zone leds are all on driver 1
		zoneAddresses[i].Input2_RedLed.DriverIndex = 1;
		// The red zone 2 leds are at output 8-15
		zoneAddresses[i].Input2_RedLed.DriverOutputChannel = i+8;
	}

	// Preset 1 Button Led is driver 2 - Channel 8
	PresetLed[0].DriverIndex = 2;
	PresetLed[0].DriverOutputChannel = 8;
	// Preset 2 Button Led is driver 2 - Channel 9
	PresetLed[1].DriverIndex = 2;
	PresetLed[1].DriverOutputChannel = 9;
	// Preset 3 Button Led is driver 2 - Channel 10
	PresetLed[2].DriverIndex = 2;
	PresetLed[2].DriverOutputChannel = 10;

	// Input 1 Control Red Led is driver 2 - Channel 11
	ControlInput1_RedLed.DriverIndex = 2;
	ControlInput1_RedLed.DriverOutputChannel = 11;

	// Input 1 Control Green Led is driver 2 - Channel 12
	ControlInput1_GreenLed.DriverIndex = 2;
	ControlInput1_GreenLed.DriverOutputChannel = 12;

	// Input 2 Control Red Led is driver 2 - Channel 13
	ControlInput2_RedLed.DriverIndex = 2;
	ControlInput2_RedLed.DriverOutputChannel = 13;

	// Input 2 Control Green Led is driver 2 - Channel 14
	ControlInput2_GreenLed.DriverIndex = 2;
	ControlInput2_GreenLed.DriverOutputChannel = 14;
}
